from pysolar.solar import *
import datetime
import math
import pandas as pd
import inspect
import tensorflow as tf
import numpy as np
import argparse
# dobj = datetime.datetime(2019,10,31,0,0,tzinfo=datetime.timezone.utc) - datetime.timedelta(hours=9)
# datetime.datetime(1993,5,5,15,50,25,0,tzinfo=datetime.timezone.utc)
# sza = float(90) - get_altitude(lat, lon, dobj)
# sza

def Koreazenith(lati,long,year,date_time):
    try:
        testing = datetime.datetime(1993,10,5,7,5,tzinfo = datetime.timezone.utc)
    except:
        print("please import datetime")
    try:
        month = int(date_time[0:2])
        day = int(date_time[2:4])
        hour = int(date_time[4:6])
        if len(date_time) == 10:
            minute = int(date_time[6:8])
            second = int(date_time[8:10])
        elif len(date_time) ==8:
            minute = int(date_time[6:8])
            second = 0
        else:
            minute = 0
            second = 0
    except:
        print("date_time should be >MonthDayHourMinuteSecond ex: 1005062300<  2 digits for each and in string form")
        print(" if year is bigger than or equal to 2018, do note write seconds")

    if year >= 2018:
        dobj = datetime.datetime(year,month,day,hour,minute, tzinfo =datetime.timezone.utc)  -  datetime.timedelta(hours = 9)
    else:
        dobj = datetime.datetime(year, month,day,hour,minute,second,0, tzinfo =datetime.timezone.utc) - datetime.timedelta(hours = 9)
    try:
        sza = float(90) - get_altitude(lati,long,dobj)
    except:
        print("please type this code at top -- from pysolar.solar import * ")
    return sza

#Koreazenith(37,120,2019,"103100")

def extract_zenith(time_asos , asos_latlon ):
    counts = range(len(time_asos))
    zenith_list = list(map(lambda x: Koreazenith( lati = float(asos_latlon[asos_latlon[:,0] == int(time_asos[x][13:len(time_asos[x])])][:,1]), ## lat
    long = float(asos_latlon[asos_latlon[:,0] == int(time_asos[x][13:len(time_asos[x])])][:,2]),       ### long
    year = int(time_asos[x][0:4]),                                                                   ### year
    date_time = time_asos[x][4:12]                 ), counts))                                      ### time and index
    return zenith_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some info.')
    parser.add_argument('-b', '--asos_bigdata',dest = "data_path",type = str, default = "pixelV_from_obstowers/pixelv_from_towers5.csv")
    parser.add_argument('-l', '--asos_latlon',dest = "latlon_path",type = str, default = "asos_data/crawled_lat_lon_towers.csv")
    parser.add_argument('-s', '--saving_path',dest = "saving_path",type = str, default = "solar_merged/solar_merged_data2.csv")
    args = parser.parse_args()
    data = pd.read_csv(args.data_path ,index_col = 0)
    lat_lon_data = pd.read_csv(args.latlon_path).values
    time = data.index
    zenith_data = extract_zenith(time_asos = time, asos_latlon = lat_lon_data)
    data['zenith'] = zenith_data
    data.to_csv(args.saving_path)
    print('done')
