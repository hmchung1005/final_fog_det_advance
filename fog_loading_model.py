import os
import pandas as pd
import json
import fog_logging
import logging
import tensorflow as tf
from fog_config import all_basic_path


def Generator(total_features_counts , OUTPUT_CHANNELS , node1 = 225 , node2 = 450 ,node3 =900 , node4 = 1800  ):
    inputs = tf.keras.layers.Input(shape=[900,900,total_features_counts])

    down_stack = [
                  downsample(node1, 4, 3, apply_batchnorm=False), # (bs, 128, 128, 64)
                  downsample(node2, 4, 3), # (bs, 64, 64, 128)
                  downsample(node3, 4, 2), # (bs, 32, 32, 256)
                  downsample(node4, 4, 2), # (bs, 16, 16, 512)
                  downsample(node4, 6, 5), # (bs, 8, 8, 512)
                  downsample(node4, 6, 5), # (bs, 4, 4, 512)
                   ]
    up_stack = [
                  upsample(node4, 6, 5, apply_dropout=True), # (bs, 8, 8, 1024)
                  upsample(node4, 4, 5), # (bs, 16, 16, 1024)
                  upsample(node3, 4, 2), # (bs, 32, 32, 512)
                  upsample(node2, 4, 2), # (bs, 64, 64, 256)
                  upsample(node1, 4 ,3), # (bs, 128, 128, 128)
                   ]

    initializer = tf.random_normal_initializer(0., 0.02)
    last = tf.keras.layers.Conv2DTranspose(OUTPUT_CHANNELS , 4,
                                         strides=3,
                                         padding='same',
                                         kernel_initializer=initializer,
                                         activation='tanh') # (bs, 256, 256, 3)
    x = inputs

    # Downsampling through the model
    skips = []
    for down in down_stack:
        x = down(x)
        skips.append(x)

    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, skip in zip(up_stack, skips):
        x = up(x)
        x = tf.keras.layers.Concatenate()([x, skip])

    x = last(x)

    return tf.keras.Model(inputs=inputs, outputs=x)
def Discriminator(total_features_counts , OUTPUT_CHANNELS , node1 = 225 , node2 = 450 , node3 = 900 , node4 = 1800):
    initializer = tf.random_normal_initializer(0., 0.02)

    inp = tf.keras.layers.Input(shape=[900, 900, total_features_counts], name='input_image')
    tar = tf.keras.layers.Input(shape=[900, 900, OUTPUT_CHANNELS], name='target_image')

    x = tf.keras.layers.concatenate([inp, tar]) # (bs, 256, 256, channels*2)

    down1 = downsample(node1, 4, 3, False)(x) # (bs, 128, 128, 64)
    down2 = downsample(node2, 4 ,3)(down1) # (bs, 64, 64, 128)
    down3 = downsample(node3, 4 ,2)(down2) # (bs, 32, 32, 256)

    zero_pad1 = tf.keras.layers.ZeroPadding2D()(down3) # (bs, 34, 34, 256)
    conv = tf.keras.layers.Conv2D(node4, 4, strides=1,
                                kernel_initializer=initializer,
                                use_bias=False)(zero_pad1) # (bs, 31, 31, 512)

    batchnorm1 = tf.keras.layers.BatchNormalization()(conv)

    leaky_relu = tf.keras.layers.LeakyReLU()(batchnorm1)

    zero_pad2 = tf.keras.layers.ZeroPadding2D()(leaky_relu) # (bs, 33, 33, 512)

    last = tf.keras.layers.Conv2D(1, 4, strides=1,
                                kernel_initializer=initializer)(zero_pad2) # (bs, 30, 30, 1)

    return tf.keras.Model(inputs=[inp, tar], outputs=last)

def downsample(filters, size,stride_size = 2 , apply_batchnorm=True):
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(tf.keras.layers.Conv2D(filters, size, strides=stride_size, padding='same',
               kernel_initializer=initializer, use_bias=False))

    if apply_batchnorm:
        result.add(tf.keras.layers.BatchNormalization())

    result.add(tf.keras.layers.LeakyReLU())

    return result

def upsample(filters, size, stride_size = 2, apply_dropout=False):
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(tf.keras.layers.Conv2DTranspose(filters, size, strides=stride_size,
               padding='same',
               kernel_initializer=initializer,
               use_bias=False))

    result.add(tf.keras.layers.BatchNormalization())

    if apply_dropout:
        result.add(tf.keras.layers.Dropout(0.5))

    result.add(tf.keras.layers.ReLU())

    return result


class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.int64):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



def main(train_data_params , log , error_log):
    OUTPUT_CHANNELS = 3
    try:
        if train_data_params["use_opt_model"] != "1":
            log.info("module init")
            saved_model_path = train_data_params["model_path"]
            img_model_dir = os.path.join(saved_model_path , "image_model")
            using_features = train_data_params["using_features"]
            total_features_counts = len(using_features)
            gan_1 = train_data_params["gan_1"]
            gan_2 = train_data_params["gan_2"]
            gan_3 = train_data_params["gan_3"]
            gan_4 = train_data_params["gan_4"]
            generator = Generator(total_features_counts=total_features_counts,
                                  OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                                  node1 = gan_1,
                                  node2 = gan_2,
                                  node3 = gan_3,
                                  node4 = gan_4)
            discriminator = Discriminator(total_features_counts=total_features_counts,
                                  OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                                  node1 = gan_1,
                                  node2 = gan_2,
                                  node3 = gan_3,
                                  node4 = gan_4)
            generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
            discriminator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
            checkpoint_dir = os.path.join(img_model_dir , "training_checkpoints")
            log.info("asserting checkpoint")

            assert len(os.listdir(checkpoint_dir)) > 1, "assertion failed"
                
            log.info("assertion success")
            checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,discriminator_optimizer=discriminator_optimizer,generator=generator,discriminator=discriminator)
            log.info("loading image model")
            checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
            log.info("checkpoint_dir : {}".format(checkpoint_dir))
            rl_generator = checkpoint.generator
            log.info("loading num model")
            num_model_dir = os.path.join(saved_model_path , "number_model")
            num_model = tf.keras.models.load_model(num_model_dir)
            length = train_data_params["num_model_pixel_length"]
            width = train_data_params["num_model_pixel_width"]

        else:
            log.info("loading opt model")
            saved_model_path = all_basic_path.backup_model_path
            img_model_dir = os.path.join(saved_model_path , "image_model")
            using_features_path = os.path.join(img_model_dir , "using_features.json")
            with open(using_features_path , "r") as fp:
                using_features = json.load(fp)
            fp.close()
            total_features_counts = len(using_features)
            with open(using_features_path , "r") as fp:
                using_features = json.load(fp)
            fp.close()
            gan_node_dic_path = os.path.join(img_model_dir , "gan_node_dic.json")
            with open(gan_node_dic_path , "r") as fp:
                gan_node_dic = json.load(fp)
            fp.close()
            gan_1 = gan_node_dic["1"]
            gan_2 = gan_node_dic["2"]
            gan_3 = gan_node_dic["3"]
            gan_4 = gan_node_dic["4"]
            generator = Generator(total_features_counts=total_features_counts,
                                  OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                                  node1 = gan_1,
                                  node2 = gan_2,
                                  node3 = gan_3,
                                  node4 = gan_4)
            discriminator = Discriminator(total_features_counts=total_features_counts,
                                  OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                                  node1 = gan_1,
                                  node2 = gan_2,
                                  node3 = gan_3,
                                  node4 = gan_4)
            generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
            discriminator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
            checkpoint_dir = os.path.join(img_model_dir , "training_checkpoints")
            checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,discriminator_optimizer=discriminator_optimizer,generator=generator,discriminator=discriminator)
            log.info("loading image model")
            checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
            rl_generator = checkpoint.generator
            log.info("loading num model")
            num_model_dir = os.path.join(saved_model_path , "number_model")
            num_model = tf.keras.models.load_model(num_model_dir)
            pixel_info_dic_path = os.path.join(num_model_dir , "pixel_info_dic.json")
            with open(pixel_info_dic_path , "r") as fp:
                pixel_info_dic = json.load(fp)
            fp.close()
            length = pixel_info_dic["length"]
            width = pixel_info_dic["width"]
    except Exception as e:
        error_log.error("loading model error : {}".format(e))
        log.info("loading previous model due to error")

        saved_model_path = all_basic_path.backup_model_path
        img_model_dir = os.path.join(saved_model_path , "image_model")


        using_features_path = os.path.join(img_model_dir , "using_features.json")
        with open(using_features_path , "r") as fp:
            using_features = json.load(fp)
        fp.close()
        total_features_counts = len(using_features)

        with open(using_features_path , "r") as fp:
            using_features = json.load(fp)
        fp.close()

        gan_node_dic_path = os.path.join(img_model_dir , "gan_node_dic.json")
        with open(gan_node_dic_path , "r") as fp:
            gan_node_dic = json.load(fp)
        fp.close()


        gan_1 = gan_node_dic["1"]
        gan_2 = gan_node_dic["2"]
        gan_3 = gan_node_dic["3"]
        gan_4 = gan_node_dic["4"]


        generator = Generator(total_features_counts=total_features_counts,
                              OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                              node1 = gan_1,
                              node2 = gan_2,
                              node3 = gan_3,
                              node4 = gan_4)

        discriminator = Discriminator(total_features_counts=total_features_counts,
                              OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                              node1 = gan_1,
                              node2 = gan_2,
                              node3 = gan_3,
                              node4 = gan_4)
        generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        discriminator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        checkpoint_dir = os.path.join(img_model_dir , "training_checkpoints")
        checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,discriminator_optimizer=discriminator_optimizer,generator=generator,discriminator=discriminator)
        log.info("loading image model")
        checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
        rl_generator = checkpoint.generator
        log.info("loading num model")
        num_model_dir = os.path.join(saved_model_path , "number_model")
        num_model = tf.keras.models.load_model(num_model_dir)


        pixel_info_dic_path = os.path.join(num_model_dir , "pixel_info_dic.json")
        with open(pixel_info_dic_path , "r") as fp:
            pixel_info_dic = json.load(fp)
        fp.close()
        length = pixel_info_dic["length"]
        width = pixel_info_dic["width"]
    finally:
        log.info("models loaded ------> ready to inference")
        return rl_generator , num_model , using_features , length , width , saved_model_path
