import sys
import json
import os
import pandas as pd
# import sqlalchemy
# from sqlalchemy import create_engine, types, select
import numpy as np
from fog_config import all_basic_path , fog_final_db_info ,fog_character, tb_info ,  gk2a_fog_img_attrs , fog_create_data_path , saving_path
#
import os
import fog_logging
import datetime
import jaydebeapi
import jpype
from PIL import Image



# 후처리 모듈에서 얻은 상태 정보를 데이터베이스 형태에 맞게 2개로 분리
def modify_to_db_shape(df):
    #df['mv_speed'] = list(map(lambda x: x.m  if 'm' in dir(x) else np.nan , df['mv_speed']))
    #df['fog_previous_id'] = list(map(lambda x: int(x),df['fog_previous_id'] ))
    result_df = df.loc[:,["fog_current_id","fog_first_id", "lat","lon","avg_vis_distance","low_vis_distance","mv_speed","mv_direction","fog_area","area_difference" , "mid_y" , "mid_x"]]
    # dir_range  = np.arange(22.5, 360.0, 45.0)
    # result_df["mv_direction"] = result_df["mv_direction"].apply(lambda row: degree_to_8dir(row, dir_range))
    return result_df



def open_connection(host,db_name,user_name,password, port, root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,
                               driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor

# 여러 개의 기지 유입 시간 변수들을 데이터베이스 형태에 맞게 한 개의 변수로 축소
def making_final_base_tibero(result, base):
    result2 = result.loc[:,['fog_current_id','fog_first_id']]
    final_base = pd.merge(result2,base, on = 'fog_current_id')
    final_base['string_time'] = list(map(lambda x: str(x)[:-3], final_base['fog_current_id'] ))
    final_base['current_time'] =  pd.to_datetime(final_base['string_time'], format = '%Y%m%d%H%M')
    final_base['inflow_time'] =  list(map(lambda x,y: x + pd.Timedelta(seconds = y), final_base['current_time'], final_base['seconds_to_reach'] ))
    final_base["inflow_time"] = final_base.inflow_time.apply(lambda x: x.strftime('%Y%m%d%H%M'))
    del final_base['string_time']
    del final_base ['current_time']
    #del final_base['fog_current_id']
    del final_base['seconds_to_reach']
    return final_base


def insert_result_tibero(final_result,ticketNo,cursor):
    final_result["now_time"] = ticketNo
    final_result["mv_speed"] = final_result["mv_speed"].clip(0,fog_character.maximum_fog_speed)

    for index in final_result.index.tolist():
        fog_current_id = final_result.loc[index,"fog_current_id"]
        fog_first_id =  final_result.loc[index,"fog_first_id"]
        lat = final_result.loc[index,"lat"]
        lon = final_result.loc[index,"lon"]
        avg_vis_distance = final_result.loc[index,"avg_vis_distance"]
        low_vis_distance = final_result.loc[index,"low_vis_distance"]
        mv_speed = final_result.loc[index,"mv_speed"]
        mv_direction = final_result.loc[index,"mv_direction"]
        fog_area = final_result.loc[index,"fog_area"]
        area_difference = final_result.loc[index,"area_difference"]
        fog_center_x = final_result.loc[index , "mid_x"]
        fog_center_y = final_result.loc[index , "mid_y"]
        now_time  = final_result.loc[index, "now_time"]
        params = (fog_current_id,fog_first_id,lat,lon,avg_vis_distance,low_vis_distance,mv_speed,mv_direction,fog_area,area_difference , fog_center_x , fog_center_y , now_time)
        sql = '''INSERT INTO d_fog_det_trac_result
        (table_id , fog_current_id,fog_first_id,lat,lon,avg_vis_distance,low_vis_distance,mv_speed,mv_direction,fog_area,area_difference , fog_center_x , fog_center_y , now_time )
        VALUES
        (D_FOG_DET_TRAC_RESULT_SEQ.NEXTVAL ,?,?,?,?,?,?,?,?,?,?,?,?,?);
        '''
        cursor.execute(sql, params)

def insert_base_tibero(final_base,ticketNo,cursor):
    final_base["now_time"] = ticketNo
    for index in final_base.index.tolist():
        fog_current_id = final_base.loc[index,"fog_current_id"]
        fog_first_id = final_base.loc[index,"fog_first_id"]
        target_base = final_base.loc[index,"target_base"]
        inflow_time = final_base.loc[index,"inflow_time"]
        now_time = final_base.loc[index , "now_time"]
        params = (fog_current_id,fog_first_id,target_base,inflow_time , now_time)
        sql ='''INSERT INTO d_fog_det_trac_base
        (table_id, fog_current_id,fog_first_id,target_base,inflow_time, now_time)
        VALUES
        (D_FOG_DET_TRAC_BASE_SEQ.NEXTVAL ,?,?,?,?,? );
        '''
        cursor.execute(sql , params)


# # 각 기지 영역의 안개 상태를 파악해서 주의보를 발령을 결정 하는 함수
def check_watch_warn_df_tibero(df):
    watch_warn_df = df.copy()
    watch_warn_df = watch_warn_df.loc[watch_warn_df["pred_value"] < fog_final_db_info.watch_warn_limit , : ] #
    watch_warn_df["watch"] = 0
    watch_warn_df["warning"] = 1
    watch_warn_df["now_time"] = watch_warn_df.now_time.apply(lambda x: x.strftime('%Y%m%d%H%M'))
    watch_warn_df["watch_warn_time"] = watch_warn_df.watch_warn_time.apply(lambda x: x.strftime('%Y%m%d%H%M'))

    return watch_warn_df



def insert_watch_warn_tibero(watch_warn,cursor):
    watch_warn_df = watch_warn.copy()
    print("inserting watch_warn")
    for index in watch_warn_df.index.tolist():
        base = watch_warn_df.loc[index , "base"]
        pred_value = watch_warn_df.loc[index, "pred_value"]
        model = watch_warn_df.loc[index, "model"]
        type = watch_warn_df.loc[index, "type"]
        watch = watch_warn_df.loc[index, "watch"]
        warning = watch_warn_df.loc[index, "warning"]
        now_time = watch_warn_df.loc[index, "now_time"]
        watch_warn_time = watch_warn_df.loc[index, "watch_warn_time"]
        sql = ''' INSERT INTO watch_warn
        (table_id , target_base, pred_value, model, type, watch, warning, now_time, watch_warn_time)
        VALUES
        (WATCH_WARN_SEQ.NEXTVAL ,{},{},'{}','{}',{},{},TO_TIMESTAMP('{}','YYYYMMDDHH24MI'),TO_TIMESTAMP('{}','YYYYMMDDHH24MI'));
        '''.format(base,pred_value,model,type,watch,warning,now_time,watch_warn_time)
        cursor.execute(sql)

# flag
def whole_validation_process(fog_det_amos_df , reading_ticketNo,ticketNo , cursor, log , level):
    amos_result = get_amos_data(reading_ticketNo , cursor, log)
    predicted = fog_det_amos_df.loc[:, ["base" , "vis_distance"]]
    validataion = merge_and_validate(predicted , amos_result,reading_ticketNo ,ticketNo  ,cursor , log, level)
    return validataion

def insert_validation_tibero(validation ,  cursor):
    now_time =validation.now_time
    GD = validation.GD
    MI = validation.MI
    FA = validation.FA
    CR = validation.CR
    POD = validation.POD
    POFD  = validation.POFD
    FART = validation.FART
    TS = validation.TS
    MAE = validation.MAE
    kinds = validation.kinds
    params =(now_time,GD,MI,FA,CR,POD,POFD,FART,TS,MAE , kinds)
    sql = '''
    INSERT INTO d_fog_det_trac_valid
    (table_id, valid_time, GD,MI,FA,CR,POD,POFD,FART,TS,MAE ,KINDS)
    VALUES
    (D_FOG_DET_TRAC_VALID_SEQ.NEXTVAL ,?,?,?,?,?,?,?,?,?,?,?);
    '''
    cursor.execute(sql , params)



def get_amos_data(ticketNo, cursor, log):
    ticketNo_kr = datetime.datetime.strptime(ticketNo, '%Y%m%d%H%M') + datetime.timedelta(minutes=540)
    ticketNo = ticketNo_kr.strftime('%Y%m%d%H%M')

    param = (ticketNo,)
    data_sql = "SELECT K2 , SU7 FROM D_AMOS_DETAIL where K1 = ? and K2 != '153';"
    data_sql_153 = "SELECT K2 , SU8 FROM D_AMOS_DETAIL where K1 = ? and K2 = '153';"
    cursor.execute(data_sql , param)
    data = cursor.fetchall()
    df = pd.DataFrame(data, columns = ["tower" , "real_vis_distance"] )

    cursor.execute(data_sql_153 , param)
    data_153 = cursor.fetchall()
    df_153 = pd.DataFrame(data_153 , columns = ["tower" , "real_vis_distance"])

    df = pd.concat([df , df_153] , ignore_index = True)

    for index in df.index.tolist():
        try:
            df.loc[index , "real_vis_distance"] = float(df.loc[index , "real_vis_distance"])
        except:
            log.info("no distance fount at AMOS BASE {}".format(str(df.loc[index, "tower"])))
            df.loc[index , "real_vis_distance"] = np.nan


    df = df.dropna()
    df = df.rename(columns = {'tower':'base'})
    df['base'] = df['base'].astype(int)
    df["real_vis_distance"] =df["real_vis_distance"].astype(int)
    return df


def get_past_valid(ticketNo , cursor):
    current_time = datetime.datetime.strptime(ticketNo,'%Y%m%d%H%M')
    past_time = current_time  - datetime.timedelta(days = fog_final_db_info.past_valid_time_limit )
    future_time = current_time +  datetime.timedelta(days = fog_final_db_info.past_valid_time_limit )
    past_ticket = datetime.datetime.strftime(past_time , format ='%Y%m%d%H%M' )
    future_ticket= datetime.datetime.strftime(future_time , format = '%Y%m%d%H%M' )
    param = (past_ticket,future_ticket)
    sql = "select GD , MI , FA , CR from d_fog_det_trac_valid where valid_time > ? and valid_time < ?;"
    cursor.execute(sql,param)
    db_data = cursor.fetchall()
    db_df = pd.DataFrame(db_data , columns = ["GD" , "MI", "FA" , "CR"] )
    db_df = db_df.dropna()
    db_df =  db_df[(db_df.iloc[:, :] >= 0).all(axis=1)]
    past_GD = sum(db_df.GD)
    past_MI = sum(db_df.MI)
    past_FA = sum(db_df.FA)
    past_CR = sum(db_df.CR)
    return past_GD , past_MI , past_FA , past_CR



def merge_and_validate(amos_info_modified , amos_result, ticketNo, saving_ticket , cursor , log , level):#fog_character.fog_binary_level
    ticketNo_kr = datetime.datetime.strptime(ticketNo, '%Y%m%d%H%M') + datetime.timedelta(minutes=540)
    ticketNo = ticketNo_kr.strftime('%Y%m%d%H%M')

    ticketNo_kr = datetime.datetime.strptime(saving_ticket, '%Y%m%d%H%M') + datetime.timedelta(minutes=540)
    saving_ticket = ticketNo_kr.strftime('%Y%m%d%H%M')

    level_to_level_num = {"shallow_fog" :  fog_character.fog_binary_level ,
                          "normal_fog" : fog_character.normal_fog_high_end,
                           "thick_fog" : fog_character.deep_fog_high_end}
    level_num = level_to_level_num[level]

    merged = amos_info_modified.merge(amos_result, on = "base")
    merged  = merged.dropna()
    merged  = merged.drop_duplicates()

    max_vis = fog_character.maximum_vis_distance_clipping
    merged = merged.loc[merged["real_vis_distance"] > 0 , :]
    log.info("merged results")
    log.info(merged)
    if merged.shape[0] > 0:
        merged["real_vis_distance"] = list(map(lambda x: max_vis if x > max_vis else x , merged["real_vis_distance"]))
        MAE = sum(abs(merged["vis_distance"] - merged["real_vis_distance"])) / merged.shape[0]

        merged["vis_distance"] = list(map(lambda x: 1 if x < level_num else 0 , merged["vis_distance"] ))
        merged["real_vis_distance"] = list(map(lambda x: 1 if x < level_num  else 0 , merged["real_vis_distance"]))
        GD = len(merged.loc[(merged["vis_distance"] ==1) & (merged["real_vis_distance"] == 1) ,:  ])
        MI = len(merged.loc[(merged["vis_distance"] ==0) & (merged["real_vis_distance"] == 1) ,:  ])
        FA = len(merged.loc[(merged["vis_distance"] ==1) & (merged["real_vis_distance"] == 0) ,:  ])
        CR = len(merged.loc[(merged["vis_distance"] ==0) & (merged["real_vis_distance"] == 0) ,:  ])
    else:
        log.info("there is no amos data --> cannot validate")
        GD , MI , FA , CR , MAE = 0,0,0,0, fog_final_db_info.null_data

    try:
        try:
            POD = GD / (GD + MI)
            POFD = FA / (FA + CR)
            FART = FA / (GD + FA)
            TS = GD / (GD + FA + MI)
        except Exception as e:
            log.info("validation error : {}".format(e))
            log.info("not enough data to validate ---> validating whole month")
            sumGD , sumMI , sumFA ,sumCR = get_past_valid(ticketNo , cursor)
            log.info("getting past valid")
            POD = sumGD / (sumGD + sumMI)
            POFD = sumFA / (sumFA + sumCR)
            FART = sumFA / (sumGD +sumFA)
            TS = sumGD / (sumGD + sumFA + sumMI)

    except Exception as e:
        print("validation unknown error : {}".format(e))
        POD = fog_final_db_info.null_data
        POFD = fog_final_db_info.null_data
        FART = fog_final_db_info.null_data
        TS = fog_final_db_info.null_data
        MAE = fog_final_db_info.null_data

    data = pd.Series({"GD":GD, "MI":MI, "FA":FA, "CR":CR,"POD":POD,"POFD":POFD,"FART":FART,"TS":TS ,"MAE":MAE , "now_time":saving_ticket  , "kinds":level } )
    return data



def insert_det_amos_df_tibero(fog_det_amos_df , cursor):
    max_vis = fog_character.maximum_vis_distance_clipping
    fog_det_amos_df["vis_distance"] = list(map(lambda x: max_vis if x >= max_vis else x , fog_det_amos_df["vis_distance"]  ))
    for index in fog_det_amos_df.index.tolist():
        now_time = fog_det_amos_df.loc[index , "now_time"]
        base = fog_det_amos_df.loc[index , "base"]
        lat = fog_det_amos_df.loc[index, "lat"]
        lon = fog_det_amos_df.loc[index , "lon"]
        vis_distance = fog_det_amos_df.loc[index , "vis_distance"]
        params = (now_time,base , lat, lon, vis_distance)
        sql =  '''INSERT INTO d_fog_det_trac_amos_detail
        (table_id , valid_time , base , lat , lon , vis_distance)
        VALUES
        (D_FOG_DET_TRAC_AMOS_DETAIL_SEQ.NEXTVAL , ?,?,?,?,?);
        '''
        cursor.execute(sql , params)

def insert_det_pred_df_tibero(fog_det_amos_df , cursor):
    max_vis = fog_character.maximum_vis_distance_clipping
    fog_det_amos_df["vis_distance"] = list(map(lambda x: max_vis if x >= max_vis else x , fog_det_amos_df["vis_distance"]  ))
    for index in fog_det_amos_df.index.tolist():
        now_time = fog_det_amos_df.loc[index , "now_time"]
        base = fog_det_amos_df.loc[index , "base"]
        vis_distance = fog_det_amos_df.loc[index , "vis_distance"]
        params = (now_time, base , vis_distance)
        sql =  '''INSERT INTO D_FOG_DET_TRAC_PRED
        (table_id , now_time , target_base , pred_value)
        VALUES
        (D_FOG_DET_TRAC_PRED_SEQ.NEXTVAL,?,?,?);
        '''
        cursor.execute(sql , params)


def read_valid_array_fog_img(reading_ticketNo, dif_rgba_list , log, error_log):
    fog_img_path = fog_create_data_path.gk2a_fog_img_path.format(reading_ticketNo[:6] , reading_ticketNo)
    raw_image = Image.open(fog_img_path)
    raw_array = np.array(raw_image)
    raw_array = raw_array[:,:,:3]
    new_array = np.full((810000,3) , gk2a_fog_img_attrs.no_diff_rgba  , dtype = "uint8")
    flattend_raw_array = np.reshape(raw_array , (810000 , 3))

    for diff_rgba in dif_rgba_list:

        diff_masked_array = (flattend_raw_array[:,0] == diff_rgba[0] ) & (flattend_raw_array[:,1] ==diff_rgba[1] ) & (flattend_raw_array[:,2] ==diff_rgba[2] )

        new_array[diff_masked_array] = diff_rgba[:3]
    valid_array = np.reshape(new_array ,  (900,900, 3))
    valid_array[22:50 , :180 , :] = [gk2a_fog_img_attrs.no_diff_rgba] * 3
    valid_array[855 : , 525:,:] = [gk2a_fog_img_attrs.no_diff_rgba] * 3
    return valid_array


def valid_diff(real_diff , pred_diff, ticketNo):
    real_diff_2d = np.reshape(real_diff , (810000 , 3))
    pred_diff_2d = np.reshape(pred_diff , (810000 , 3))

    real_diff_binary = np.sum(real_diff_2d , axis  = 1)
    pred_diff_binary = np.sum(pred_diff_2d , axis  = 1)

    # 바이너리 형태로 변경
    real_diff_binary = real_diff_binary.clip(0,1).tolist()
    pred_diff_binary = pred_diff_binary.clip(0,1).tolist()

    GD = sum([ x==1 and y == 1 for x,y in zip(real_diff_binary , pred_diff_binary) ])
    MI = sum([ x==1 and y == 0 for x,y in zip(real_diff_binary , pred_diff_binary) ])
    FA = sum([ x==0 and y == 1 for x,y in zip(real_diff_binary , pred_diff_binary) ])
    CR = sum([ x==0 and y == 0 for x,y in zip(real_diff_binary , pred_diff_binary) ])

    MAE = -50000
    level = "diff"
    POD = GD / (GD + MI)
    POFD = FA / (FA + CR)
    FART = FA / (GD + FA)
    TS = GD / (GD + FA + MI)
    data = pd.Series({"GD":GD, "MI":MI, "FA":FA, "CR":CR,"POD":POD,"POFD":POFD,"FART":FART,"TS":TS ,"MAE":MAE , "now_time":ticketNo  , "kinds":level } )
    return data






#----------------------------------------------------------------------------------------------------------------------------------------------------------
#flag
def main(postprocess_params , watch_warn_df_params, pred_diff ,log , error_log):

    output_path = all_basic_path.output_root_path
    root_path = all_basic_path.root_source_path
    tempo_path = saving_path.tempo_path


    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port
    log.info("setting done")

    try:
        conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)
        log.info("open_connection")
    except Exception as e:
        error_log.error("open_connection : {}".format(e))
    log.info("setting done")

    try:
        final_result = postprocess_params["result_db"]
        base_result = postprocess_params["base_db"]
        ticketNo = postprocess_params["ticketNo"]
        reading_ticketNo = postprocess_params["reading_ticketNo"]
        watch_warn_df = watch_warn_df_params["watch_warn_df"]
        fog_det_amos_df = watch_warn_df_params["amos_detail_df"]
        valid_det_amos_df = watch_warn_df_params["valid_detail_df"]
        # 해무 추적 결과 정보 데이터 생성
        final_result = modify_to_db_shape(final_result)
    except Exception as e:
        error_log.error("modify_to_db_shape : {}".format(e))

    try:
        # 기지 유입 정보 데이터 생성
        final_base = making_final_base_tibero(final_result,base_result)
    except Exception as e:
        error_log.error("making_final_base_tibero : {}".format(e))

    try:
        # 해무 추적 결과 정보 데이터베이스에 삽입
        insert_result_tibero(final_result, ticketNo,cursor)
        log.info("insert_result_tibero")
    except Exception as e:
        error_log.error("insert_result_tibero : {}".format(e))

    try:
        # 기지 유입 정보 데이터베이스에 삽입
        insert_base_tibero(final_base,ticketNo ,cursor)
        log.info("insert_base_tibero")
    except Exception as e:
        error_log.error("insert_base_tibero : {}".format(e))

    try:
        # 주의보 경보 데이터 생성
        watch_warn_df= check_watch_warn_df_tibero(watch_warn_df)
        log.info("check_watch_warn_df_tibero")
    except Exception as e:
        error_log.error("chekc_watch_warn_df_tibero : {}".format(e))

    try:
        if watch_warn_df.shape[0] > 0:
            # 주의보 경보 정보 데이터베이스에 삽입
            insert_watch_warn_tibero(watch_warn_df,cursor)
            log.info("inset_watch_warn_tibero")
        else:
            log.info("no warning signs")
    except Exception as e:
        error_log.error("insert_watch_warn_tibero : {}".format(e))

    # try:
    #     insert_det_amos_df_tibero(fog_det_amos_df , cursor)
    #     log.info("insert_det_amos_df_tibero")
    # except Exception as e:
    #     error_log.error("insert_det_amos_df_tibero".format(e) )


    try:
        # 기지 및 관할지점 결과 정보 데이터베이스에 삽입
        insert_det_pred_df_tibero(fog_det_amos_df , cursor)
        log.info("insert_det_pred_tibero")
    except Exception as e:
        error_log.error("insert_det_pred_tibero".format(e) )


    try:
        level_list = ["shallow_fog" , "normal_fog" , "thick_fog"]
        for level in level_list:
            # 발달 단계별로 검증
            validataion = whole_validation_process(valid_det_amos_df,reading_ticketNo,ticketNo  , cursor,log , level)
            # 검증 결과 데이터베이스에 삽입
            insert_validation_tibero(validataion ,  cursor)

            log.info("whole_validation_process")
    except Exception as e:
        error_log.error("whole_validation_process error : {}".format(e))
    try:

        dif_rgba_list = [gk2a_fog_img_attrs.diff1_rgba,
                          gk2a_fog_img_attrs.diff2_rgba,
                          gk2a_fog_img_attrs.diff3_rgba,
                          gk2a_fog_img_attrs.diff4_rgba,
                          gk2a_fog_img_attrs.diff5_rgba,
                          gk2a_fog_img_attrs.diff6_rgba]
        # 실제 Diff 이미지 읽기
        real_diff = read_valid_array_fog_img(reading_ticketNo, dif_rgba_list , log, error_log)
        pred_diff = pred_diff[:,:,:3]
        # Diff 이미지 검증 결과 생성
        diff_valid = valid_diff(real_diff ,pred_diff, ticketNo)
        # Diff 이미지 검증 결과 데이터베이스에 삽입
        insert_validation_tibero(diff_valid, cursor)
        log.info("valid diff")
    except Exception as e:
        error_log.error("valid_diff error : {}".format(e))

    cursor.close()
    conn.close()
    log.info("whole process finished")
    try:
        log.info("handling image threaded")
        import fog_det_handling_img
        log.info('fog_handling start')
        # 데이터베이스 와 추론 결과 이미지를 활용해서 핸들링 이미지 생성
        fog_det_handling_img.main(ticketNo)

        log.info('fog_handling end')
    except Exception as e:
        error_log.error("handling image thread call error : {}".format(e))
    print("done")
