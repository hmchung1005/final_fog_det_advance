import pandas as pd
import numpy as np
import requests
from hdfs import InsecureClient
import fog_logging
import json
import os
from fog_config  import preprocess_info , fog_create_data_path , saving_path , gk2a_fog_img_attrs, fog_character, hdfs_path_class
from datetime import date, datetime, timedelta , timezone
from pysolar import solar
import tensorflow as tf
import scipy.spatial.distance
from PIL import Image
from matplotlib import pyplot as plt


def dbz_to_r(dBz):
    Z = idecibel(dBz)
    R = z_to_r(Z, a=200, b=1.6)
    R = np.round(R, 5)
    return R

def idecibel(x):

    return 10.0 ** (x / 10.0)


def z_to_r(z, a=200.0, b=1.6):

    return (z / a) ** (1.0 / b)


def getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user):
    active_host = None

    #  ambari user/pass
    #auth_user = 'admin'

    #auth_pass = 'admin'
    #ambari_host = "192.168.0.174"
    #  ambari host
    #ambari_cluster_name = 'hacluster'


    api_url = 'http://{}:8080/api/v1/clusters/{}/host_components?HostRoles/component_name=NAMENODE&metrics/dfs/FSNamesystem/HAState=active'.format(ambari_host,
                                                                                                                                                   ambari_cluster_name)
    res = requests.get(api_url, auth=(auth_user, auth_pass))

    status_code = res.status_code
    res_json = res.json()

    # - status 200 == 정상
    # - status 403 == 암바리 계정명 불일치
    # - status 404 == api url 재확인 필요
    # - status 5xx == 암바리 서버 에러

    if status_code==200 and res_json['items']:
        active_host = res_json['items'][0]['HostRoles']['host_name']

    return status_code, active_host


#@@@
def single_x_data_nas(ticketNo , using_features ,hsr_merge_table ,latlon_900_2d , log , error_log , features_min_max_dic):
    x_data = np.zeros((900, 900 , len(using_features)), dtype ="float32" )
    try:
        for order ,feature in enumerate(using_features):
            log.info(feature)
            if feature == "HSR_value":
                temp_array = read_hsr_array_nas(ticketNo , hsr_merge_table , log , error_log)
            elif feature == "zenith":
                temp_array = create_zenith_array(ticketNo , latlon_900_2d , log , error_log)
            else:
                temp_array = read_gk2a_x_array_nas(ticketNo , feature , log , error_log)
            x_data[: , : , order] = temp_array
        x_data = normalize_one_array(x_data , using_features , features_min_max_dic)
        return x_data
    except Exception as e:
        error_log.error("error reading ticket at : {} ----> {}".format(ticketNo , e))


def single_x_data_hdfs(ticketNo , using_features ,hsr_merge_table ,latlon_900_2d  , log , error_log ,features_min_max_dic):
    x_data = np.zeros((900, 900 , len(using_features))  )
    try:
        for order , feature in enumerate(using_features):
            log.info(feature)
            if feature == "HSR_value":
                temp_array = read_hsr_array_hdfs(ticketNo , hsr_merge_table , log , error_log)
            elif feature == "zenith":
                temp_array = create_zenith_array(ticketNo , latlon_900_2d , log , error_log)
            else:
                temp_array = read_gk2a_x_array_hdfs(ticketNo , feature , log , error_log)
            x_data[:, : , order ] = temp_array
        x_data = normalize_one_array(x_data , using_features , features_min_max_dic)
        return x_data
    except Exception as e:
        error_log.error("error reading ticket at : {} --->{}".format(ticketNo , e))

def normalize_one_array(x_data , using_features , features_min_max_dic):
    for i , feature in enumerate(using_features):
        temp_min = features_min_max_dic[feature][0]
        temp_max = features_min_max_dic[feature][1]
        x_data[:,:,i] = (x_data[:,:,i] - temp_min) / (temp_max - temp_min)
    return x_data

# 태양 천정각 빠르게 계산
def create_zenith_array(ticketNo , latlon_900_2d , log , error_log ):
    # try:
    kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
    year = int(kr_ticketNo[:4])
    month = int(kr_ticketNo[4:6])
    day = int(kr_ticketNo[6:8])
    hour = int(kr_ticketNo[8:10])
    minute = int(kr_ticketNo[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime(year,month,day,hour,minute, tzinfo =timezone.utc)  - timedelta(hours = 9)
    temp_array = get_altitude_faster(latlon_900_2d, dobj)
    # 900 900 2 차원 배열로 만듬
    whole_array = np.reshape(temp_array, (900,900) )
    final_array = float(90) - whole_array
    log.info("zenith created")
    return final_array



def read_hsr_array_nas(ticketNo , hsr_merge_table, log ,error_log):
    # try:
    kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
    kr_yearmonth = kr_ticketNo[:6]
    kr_day = kr_ticketNo[6:8]
    file_path = fog_create_data_path.hsr_array_nas_path.format(kr_yearmonth , kr_day , kr_ticketNo, kr_ticketNo)
    temp_array = pd.read_csv(file_path , header = None ).iloc[:,:-1].values
    hsr_y_list = hsr_merge_table["hsr_y"].values
    hsr_x_list = hsr_merge_table["hsr_x"].values
    gk2a_y_list = hsr_merge_table["gk2a_y"].values
    gk2a_x_list = hsr_merge_table["gk2a_x"].values
    returning_array = np.full((900,900),-200)
    inserting_values = temp_array[hsr_y_list , hsr_x_list]
    returning_array[gk2a_y_list, gk2a_x_list] = inserting_values
    rain_np = dbz_to_r(returning_array)
    log.info("HSR_Value loaded")
    return rain_np

# yearmonth #day #hour #size #ticket
def read_gk2a_x_array_nas(ticketNo , feature, log ,error_log):
    # try:
    yearmonth = ticketNo[:6]
    day = ticketNo[6:8]
    hour = ticketNo[8:10]
    channel = feature.split("_")[0]
    if "vi006" in feature:
        size = "05"
    elif "vi" in feature:
        size = "10"
    else:
        size = "20"
    folder_path = fog_create_data_path.gk2a_array_nas_folder.format(yearmonth , day , hour, channel ,size , ticketNo)
    file_list = os.listdir(folder_path)
    key_word1 =  "nc." + feature.split("_")[1]
    key_word2 =  "nc" + feature.split("_")[1]
    available_file = list(filter(lambda x: key_word1 in x or key_word2 in x , file_list ))[0]
    final_file_path = os.path.join(folder_path , available_file)
    if final_file_path.endswith("npz"):
        temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
    else:
        temp = pd.read_csv(final_file_path ,header = None ,chunksize = 1000)
        temp_df = temp.read()
        log.info("using chunksize")
        temp_array = temp_df.values

    wide_len = temp_array.shape[1]
    if wide_len == 3601 or wide_len == 1801 or wide_len == 901:
        log.info("extra line at the array ----> deleting last line")
        temp_array = temp_array[: , : -1]
    if temp_array[0 , -1] == " " or temp_array[1 , -1] == " ":
        log.info("parsing error ---> shifting values to right by 1 pixel")
        temp_array[: ,1:] = temp_array[:, :-1]
    try:
        temp_array = temp_array.astype("float32")
    except Exception as e:
        log.info("error at type as float : {}".format(e))
        y_length = temp_array.shape[0]
        x_length = temp_array.shape[1]
        temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
        temp_array = np.reshape(temp_array  , (y_length , x_length) )
        temp_array = temp_array.astype("float")
        log.info("success making it numeric")

    if "vi006" in feature:
        try:
            temp_array = transform_gk_3600_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_3600_to_900 ----> {}".format(e))
    elif "vi" in feature:
        try:
            temp_array = transform_gk_1800_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_1800_to_900 ----> {}".format(e))

    log.info("success loading {} : {}".format(ticketNo , feature))

    if np.isnan(np.sum(temp_array)):
        log.info("nan value found in the array ---> masking array")
        mask = np.isnan(temp_array)
        temp_array[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), temp_array[~mask])
        log.info('masking succsss')
    return temp_array

def read_hsr_array_hdfs(ticketNo , hsr_merge_table, log ,error_log):
    # try:
    kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
    kr_yearmonth = kr_ticketNo[:6]
    kr_day = kr_ticketNo[6:8]

    ambari_host = hdfs_path_class.ambari_host
    auth_pass = hdfs_path_class.auth_pass
    ambari_cluster_name = hdfs_path_class.ambari_cluster_name
    auth_user = hdfs_path_class.auth_user

    hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass ,ambari_cluster_name ,auth_user)

    if hdfs_status != 200:
        log.info("hdfs status : {}".format(hdfs_status))

    working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)

    file_path = fog_create_data_path.hsr_array_hdfs_path.format(kr_yearmonth , kr_day , kr_ticketNo)

    client = InsecureClient(working_hdfs_path, user="hdfs")
    status = client.status(file_path,  strict = False)
    if status  == None:
        log.info("no file found at : {}".format(file_path))

    with client.read(file_path) as file:
        temp_array = pd.read_csv(file, header = None ).iloc[:,:-1].values

    hsr_y_list = hsr_merge_table["hsr_y"].values
    hsr_x_list = hsr_merge_table["hsr_x"].values
    gk2a_y_list = hsr_merge_table["gk2a_y"].values
    gk2a_x_list = hsr_merge_table["gk2a_x"].values
    returning_array = np.full((900,900),-200)
    inserting_values = temp_array[hsr_y_list , hsr_x_list]
    returning_array[gk2a_y_list, gk2a_x_list] = inserting_values
    rain_np = dbz_to_r(returning_array)
    log.info("HSR_Value loaded")
    return rain_np


def read_gk2a_x_array_hdfs(ticketNo , feature, log ,error_log):
    yearmonth = ticketNo[:6]
    day = ticketNo[6:8]

    channel , feature_type = feature.split("_")

    ambari_host = hdfs_path_class.ambari_host
    auth_pass = hdfs_path_class.auth_pass
    ambari_cluster_name = hdfs_path_class.ambari_cluster_name
    auth_user = hdfs_path_class.auth_user
    hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host, auth_pass , ambari_cluster_name,auth_user )
    if hdfs_status  !=  200:
        log.info("hdfs status : {}".format(hdfs_status))

    if "vi006" in feature:
        size = "05"
    elif "vi" in feature:
        size = "10"
    else:
        size = "20"
    if feature_type == "Brightness" : feature_type = "Brightness_Temperature"


    file_path = fog_create_data_path.gk2a_array_hdfs_path.format(channel.upper() ,yearmonth , day ,  channel , size , ticketNo , feature_type)
    #capital channel #yearmonth  #day #channel #size #ticketNo  #feature (Brightness_Temperature)

    working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)

    client = InsecureClient(working_hdfs_path, user="hdfs")
    file_status = client.status(file_path,  strict = False)
    if file_status  == None:
        log.info("no file found at : {}".format(file_path))
    with client.read(file_path) as file:
        temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values

    wide_len = temp_array.shape[1]
    if wide_len == 3601 or wide_len == 1801 or wide_len == 901:
        log.info("extra line at the array ----> deleting last line")
        temp_array = temp_array[: , : -1]
    if temp_array[0 , -1] == " " or temp_array[1 , -1] == " ":
        log.info("parsing error ---> shifting values to right by 1 pixel")
        temp_array[: ,1:] = temp_array[:, :-1]
    try:
        temp_array = temp_array.astype("float")
    except Exception as e:
        log.info("error at type as float : {}".format(e))
        y_length = temp_array.shape[0]
        x_length = temp_array.shape[1]
        temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
        temp_array = np.reshape(temp_array  , (y_length , x_length) )
        temp_array = temp_array.astype("float")
        log.info("success making it numeric")
    if "vi006" in feature:
        try:
            temp_array = transform_gk_3600_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_3600_to_900 ----> {}".format(e))
    elif "vi" in feature:
        try:
            temp_array = transform_gk_1800_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_1800_to_900 ----> {}".format(e))
    log.info("success loading {} : {}".format(ticketNo , feature))
    if np.isnan(np.sum(temp_array)):
        log.info("nan value found in the array ---> masking array")
        mask = np.isnan(temp_array)
        temp_array[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), temp_array[~mask])
        log.info('masking succsss')
    return temp_array


def transform_gk_3600_to_900(array_3600):
    array_list = []
    for i in range(4):
        for j in range(4):
            temp_array = array_3600[i::4 ,j::4]
            array_list.append(temp_array)

    final_array = sum(array_list) / 16
    return final_array

def transform_gk_1800_to_900(array_1800):
    a = array_1800[::2, ::2]
    b = array_1800[::2, 1::2]
    c = array_1800[1::2, ::2]
    d = array_1800[1::2, 1::2]
    sum = a +b+ c+d
    result_array = sum / 4
    return result_array

# 태양 고도 빠르게 계산 함수
def get_altitude_faster(latlon_900_2d, when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(latlon_900_2d[:,0])
    hour_angle = solar.get_hour_angle(when, latlon_900_2d[:,1] )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))

def set_gpu(gpu_num):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')   # 물리적 GPU리스트
    if physical_devices != []:
        tf.config.experimental.set_visible_devices(physical_devices[gpu_num], 'GPU')   # 3번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.experimental.set_memory_growth(physical_devices[gpu_num], True)



def save_diff_image(final_pred_array , saving_ticketNo , subjects , mapping_dic):
    ticketNo_kr = datetime.strptime(saving_ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    saving_ticketNo = ticketNo_kr.strftime('%Y%m%d%H%M')

    oned_result = np.reshape(final_pred_array , (810000 , 3) )
    mapping = scipy.spatial.distance.cdist(oned_result, subjects).argmin(1)
    final_oned = [mapping_dic[x] for x in mapping]
    final_2d = np.reshape(final_oned , (900,900, 4))
    final_diff = final_2d.astype("uint8")

    yearmonthday = saving_ticketNo[:8]
    diff_img_saving_dir = os.path.join(saving_path.diff_img_path , yearmonthday)
    if not os.path.exists(diff_img_saving_dir):
        os.makedirs(diff_img_saving_dir)
    diff_img_saving_path = os.path.join(diff_img_saving_dir , "fog-det-trac_{}_diff-image-det.png".format(saving_ticketNo))
    final_diff_image = Image.fromarray(final_diff)
    final_diff_image.save(diff_img_saving_path)
    return final_diff



def main(ticketNo_dic ,rl_generator ,num_model , using_features , length , width  , log , error_log):
    # 나스 hdfs 선택
    if preprocess_info.hdfs_nas == "nas":
        read_x_data = single_x_data_nas
    else:
        read_x_data = single_x_data_hdfs

    # hsr 매핑 데이터프레임
    hsr_mapping_table = pd.read_csv(fog_create_data_path.hsr_mapping_table)
    hsr_merge_table = hsr_mapping_table.loc[(hsr_mapping_table['hsr_y'] >= 0 ) & (hsr_mapping_table['hsr_y'] <= 2880) & (hsr_mapping_table['hsr_x'] >= 0 ) & (hsr_mapping_table['hsr_x'] <= 2304 ),: ]
    # gk2a 픽셀 별 위도 경도 데이터프레임
    latlon_900_900 = pd.read_csv(fog_create_data_path.latlon_900_900,header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)

    # 읽는 데이터의 ticketNo
    ticketNo =  ticketNo_dic["reading_ticketNo"]
    # 저장 하는 ticketNo
    saving_ticketNo = ticketNo_dic["saving_ticketNo"]
    features_min_max_dic = preprocess_info.features_min_max
    log.info("loading data")
    # 데이터 읽어와서 배열 형태로 변환
    x_np = read_x_data(ticketNo , using_features ,hsr_merge_table ,latlon_900_2d  , log , error_log , features_min_max_dic)
    log.info("loading data done")
    x_np  = x_np[tf.newaxis , ...]

    log.info("img model started prediction")
    # 생성자 모델로 추론
    prediction = rl_generator(x_np , training =  True)
    log.info("img model finished prediction")
    prediction = prediction[0]
    prediction = np.array(prediction)
    # rgb 형태로 만듬
    prediction_rgb = prediction * 255.0
    prediction_rgb =  np.clip(prediction_rgb , 0 , 255)
    final_pred_array = np.array(prediction_rgb , dtype = "uint8")
    subjects = np.array([gk2a_fog_img_attrs.diff1_rgba[:3]  ,
                         gk2a_fog_img_attrs.diff2_rgba[:3]  ,
                         gk2a_fog_img_attrs.diff3_rgba[:3]  ,
                         gk2a_fog_img_attrs.diff4_rgba[:3]  ,
                         gk2a_fog_img_attrs.diff5_rgba[:3]  ,
                         gk2a_fog_img_attrs.diff6_rgba[:3]  ,
                         [gk2a_fog_img_attrs.no_diff_rgba] * 3    ])

    mapping_dic = {0:gk2a_fog_img_attrs.diff1_rgba,
                   1:gk2a_fog_img_attrs.diff2_rgba,
                   2:gk2a_fog_img_attrs.diff3_rgba,
                   3:gk2a_fog_img_attrs.diff4_rgba,
                   4:gk2a_fog_img_attrs.diff5_rgba,
                   5:gk2a_fog_img_attrs.diff6_rgba,
                   6: [gk2a_fog_img_attrs.no_diff_rgba] * 4  }

    # diff 이미지 생성. ---> 시정 거리 이미지는 수치 모델 추론 이후 따로 생성하고 현재 ui 에서는 시정거리 이미지를 사용
    final_diff = save_diff_image(final_pred_array , saving_ticketNo , subjects , mapping_dic)

    # 수치 모델 데이터 별 전치 세로 가로 길이. 가운데
    total_length = length * 2 + 1
    total_width = width * 2 + 1

    # 수치 모델 입력 데이터 배열 형태
    sub_shape = (total_length,total_width , 3)

    # 이미지 모델 추론 결과 배열을 r g b 배열 따로 분리
    r_arr = prediction[:,:,0]
    g_arr = prediction[:,:,1]
    b_arr = prediction[:,:,2]

    # 각 배열 마다 입력 데이터 세로 가로 길이 별로 스트라이딩
    r_window = np.lib.stride_tricks.sliding_window_view(r_arr , sub_shape[:-1])
    g_window = np.lib.stride_tricks.sliding_window_view(g_arr , sub_shape[:-1])
    b_window = np.lib.stride_tricks.sliding_window_view(b_arr , sub_shape[:-1])

    final_shape = b_window.shape[:2] + sub_shape

    final_input_array = np.zeros(final_shape)

    # 데이터 융합
    final_input_array[:,:,:,:,0] = r_window
    final_input_array[:,:,:,:,1] = g_window
    final_input_array[:,:,:,:,2] = b_window


    total_counts = b_window.shape[0] * b_window.shape[1]

    final_input_array = np.reshape(final_input_array , ( (total_counts,) + sub_shape   )  )

    log.info("num model started prediction")
    # 수치 모델 추론 시작
    num_model_results = num_model.predict(final_input_array)
    log.info("num model finished prediction")
    results_array = np.reshape( num_model_results , ( b_window.shape[0] ,  b_window.shape[1])     )
    results_array = np.clip(results_array , 0 , fog_character.maximum_vis_distance_clipping)
    final_results_array = np.full((900,900) , fog_character.maximum_vis_distance_clipping)
    # 추론 결과 이미지 900 900 배열에 삽입
    final_results_array[length : (899-length +1) , width : (899 - width + 1) ] = results_array

    return final_results_array , final_diff
