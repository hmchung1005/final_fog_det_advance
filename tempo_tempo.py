
import tensorflow as tf
import numpy as np


def npy_to_tfrecords(X,Y , output_file):
    # write records to a tfrecords file
    writer = tf.io.TFRecordWriter(output_file)
    # Feature contains a map of string to feature proto objects
    for i in range(len(X)):
        feature = {}
        feature['X'] = tf.train.Feature(float_list=tf.train.FloatList(value=X[i] ) )
        feature['Y'] = tf.train.Feature(float_list=tf.train.FloatList(value=Y[i] ) )

        # Construct the Example proto object
        example = tf.train.Example(features=tf.train.Features(feature=feature))

        # Serialize the example to a string
        serialized = example.SerializeToString()

        # write the serialized objec to the disk
        writer.write(serialized)
    writer.close()


xxr = np.zeros((2,900))
yyr = np.zeros((2,900))

print('Writing')
npy_to_tfrecords(xxr, yyr, "testing22.tfrecords")
