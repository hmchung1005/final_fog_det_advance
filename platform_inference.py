import sys
import os
sys.path.append('/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance')
import tensorflow as tf
import fog_logging
import numpy as np
import fog_preprocess
import fog_data_input_validation     ##################
import fog_postprocess
import logging
import fog_database
from datetime import datetime, timedelta
import fog_config


def train(tm):
    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_train_info')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_train_error')
    try:
        # Disable all GPUS
        tf.config.experimental.set_visible_devices([], 'GPU')
        visible_devices = tf.config.experimental.get_visible_devices()
        for device in visible_devices:
            assert device.device_type != 'GPU'
        log.info('train set cpu succeed')
    except Exception as e:
        # Invalid device or cannot modify virtual devices once initialized.
        error_log.error('set cpu error : {}'.format(e))

    try:
        import fog_create_data
        import fog_create_img_model
        import fog_create_num_model
        log.info('train starts')
        log.info('tm model path {}'.format(tm.model_path))
        param = tm.param_info
        log.info('prams ->{}'.format(param))
        all_features = list(fog_config.preprocess_info.features_min_max.keys())
        using_features = []
        for feature in all_features:
            if str(param[feature]) == "2":
                using_features.append(feature)
        log.info("using_features : {}".format(using_features))
        train_data_params = {"train_start_ticket"    :  str(param['train_start_ticket']),
                             "train_end_ticket"      :  str(param['train_end_ticket']),
                             "using_features"              :  using_features,
                             "model_path"                  :  tm.model_path,
                             "gan_1"                       :  int(param["gan_1"]),
                             "gan_2"                       :  int(param["gan_2"]),
                             "gan_3"                       :  int(param["gan_3"]),
                             "gan_4"                       :  int(param["gan_4"]),
                             "dnn_1"                       :  int(param["dnn_1"]),
                             "dnn_2"                       :  int(param["dnn_2"]),
                             "dnn_3"                       :  int(param["dnn_3"]),
                             "optimizer"                   :  str(param["optimizer"]),
                             "loss_func"                   :  str(param["loss_func"]),
                             "img_model_test_data_counts"  :  int(param["img_model_test_data_counts"]) ,
                             "num_model_test_data_ratio"   :  float(param["num_model_test_data_ratio"]),
                             "num_model_pixel_width"       :  int(param["num_model_pixel_width"]),
                             "num_model_pixel_length"      :  int(param["num_model_pixel_length"]),
                             "batch_size_num"              :  int(param["batch_size"]),
                             "epoch_num"                   :  int(param["epoch"]),
                             "learning_rate"               :  float(param["learning_rate"]),
                             "es_patience"                 :  int(param["es_patience"]),
                             "use_opt_model"               :  str(param["use_opt_model"])}
        if str(param["use_opt_model"]) != "1":
            try:
                log.info("creating data")
                img_model_dir , training_data_dir , using_features_path = fog_create_data.main(train_data_params , log ,error_log)
                log.info("done creating data")
                log.info("training img model")
                num_model_data_dic = fog_create_img_model.main(train_data_params  , img_model_dir , training_data_dir, log , error_log)
                log.info("training num model")
                fog_create_num_model.main(num_model_data_dic , train_data_params, log , error_log)
                log.info("training done")
            except Exception as e:
                error_log.error("error fog_create_data {}".format(e))
        else:
            log.info("using opt model ---------> skip training")
    except Exception as e:
        error_log.error("error : {}".format(e))


def init_svc(im):
    import fog_loading_model  ################
    import json
    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
    try:
        # Disable all GPUS
        tf.config.experimental.set_visible_devices([], 'GPU')
        visible_devices = tf.config.experimental.get_visible_devices()
        for device in visible_devices:
            assert device.device_type != 'GPU'
        log.info('train set cpu succeed')
    except Exception as e:
        # Invalid device or cannot modify virtual devices once initialized.
        error_log.error('set cpu error : {}'.format(e))

    model_path = im.model_path
    param = im.param_info
    all_features = list(fog_config.preprocess_info.features_min_max.keys())
    using_features = []
    for feature in all_features:
        if str(param[feature]) == "2":
            using_features.append(feature)
    log.info("using_features : {}".format(using_features))
    train_data_params = {"train_start_ticket"    :  str(param['train_start_ticket']),
                         "train_end_ticket"      :  str(param['train_end_ticket']),
                         "using_features"              :  using_features,
                         "model_path"                  :  im.model_path,
                         "gan_1"                       :  int(param["gan_1"]),
                         "gan_2"                       :  int(param["gan_2"]),
                         "gan_3"                       :  int(param["gan_3"]),
                         "gan_4"                       :  int(param["gan_4"]),
                         "dnn_1"                       :  int(param["dnn_1"]),
                         "dnn_2"                       :  int(param["dnn_2"]),
                         "dnn_3"                       :  int(param["dnn_3"]),
                         "optimizer"                   :  str(param["optimizer"]),
                         "loss_func"                   :  str(param["loss_func"]),
                         "img_model_test_data_counts"  :  int(param["img_model_test_data_counts"]) ,
                         "num_model_test_data_ratio"   :  float(param["num_model_test_data_ratio"]),
                         "num_model_pixel_width"       :  int(param["num_model_pixel_width"]),
                         "num_model_pixel_length"      :  int(param["num_model_pixel_length"]),
                         "batch_size_num"              :  int(param["batch_size"]),
                         "epoch_num"                   :  int(param["epoch"]),
                         "learning_rate"               :  float(param["learning_rate"]),
                         "es_patience"                 :  int(param["es_patience"]),
                         "use_opt_model"               :  str(param["use_opt_model"])}


    log.info('model path ->{}'.format(model_path))
    log.info('model path exists : {}'.format(os.path.exists(model_path)))
    try:
        rl_generator , num_model , using_features , length , width , loading_model_path= fog_loading_model.main(train_data_params,log, error_log)
        log.info('model loaded')
    except Exception as e:
        error_log.error('model loading error :{}'.format(e))
    try:
        processing_path = '{}/processing_status.json'.format(model_path)
        status_dic= {"processing" : "False"}
        with open(processing_path, 'w') as f:
            json.dump(status_dic, f, indent=4)
        f.close()
    except Exception as e:
        error_log.error("writing json error : {}".format(e))


    params = {"rl_generator" : rl_generator,
              "num_model" : num_model,
              "using_features" : using_features,
              "length" : length,
              "width"  : width,
              "loading_model_dir" : loading_model_path,
              "model_path" : model_path}

    return params

def data_input_validation(ticketNo):
    '''check data'''


def inference(df, params, batch_size):
    log = fog_logging.get_log_view(1, "platform", False, 'inference_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'inference_error_log')

    log.info('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    ticketNo = ticket_df.tolist()[0]
    log.info('ticketNo : {}'.format(ticketNo))
    using_features = params["using_features"]
    model_path = params["model_path"]
    is_null , reading_ticketNo = fog_data_input_validation.main(ticketNo , using_features, model_path)


    if is_null:
        return False
    else:
        log.info('call_threadfunc start')
        ticketNo_dic = {"reading_ticketNo": reading_ticketNo ,
                        "saving_ticketNo" : ticketNo}
        call_threadfunc(ticketNo_dic, log, error_log , params)
        log.info('inference end')
        return True

def call_threadfunc(ticketNo_dic, log, error_log, params):

    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)

    try:
        ticket_tuple = (ticketNo_dic, log, error_log,  params)
        future = pool.submit(processing_after_call,(ticket_tuple))
    except Exception as e:
        error_log.error('processing_after_call error : {}'.format(e))

    log.info('call_threadfunc end')

def processing_after_call(ticket_tuple):
    ticketNo_dic, log, error_log, params = ticket_tuple
    import json
    model_path = params["model_path"]
    processing_path = '{}/processing_status.json'.format(model_path)
    with open(processing_path) as json_file:
        status = json.load(json_file)
    json_file.close()

    if status["processing"] == "False":
        try:
            status["processing"] = "True"
            with open(processing_path, 'w') as f:
                json.dump(status, f, indent=4)
            f.close()
            rl_generator = params["rl_generator"]
            num_model = params["num_model"]
            using_features = params["using_features"]
            length = params["length"]
            width = params["width"]
            loading_model_dir = params["loading_model_dir"]
            model_path = params["model_path"]

            final_results_array , pred_diff  = fog_preprocess.main(ticketNo_dic ,rl_generator ,num_model , using_features , length , width  , log , error_log)
            postprocess_params , watch_warn_df_params = fog_postprocess.main(ticketNo_dic, final_results_array , log , error_log)
            fog_database.main(postprocess_params , watch_warn_df_params, pred_diff ,log , error_log)
            print("process finished")
        except Exception as e:
            error_log.error(e)
    else:
        log.info("previous process is not finished-----> stopping process")

    status["processing"] = "False"
    with open(processing_path, 'w') as f:
        json.dump(status, f, indent=4)
    f.close()
    log.info('status changed')




##
from  fog_create_img_model import set_gpu

#

class making_class:
    def __init__(self , path , info ):
        self.model_path = path
        self.param_info = info


if __name__ == '__main__':
    #set_gpu(2)
    log = fog_logging.get_log_view(1, "platform", False, 'inference_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'inference_error_log')

    info = {             "train_start_ticket"        :"201910100000",
                         "train_end_ticket"          :"201910100050",
                         'ir087_Radiance'            : "2" ,
                         'ir087_Brightness'          : "1",
                         'ir096_Radiance'            : "2",
                         'ir096_Brightness'          : "1",
                         'ir105_Radiance'            : "2",
                         'ir105_Brightness'          : "1",
                         'ir112_Radiance'            : "2",
                         'ir112_Brightness'          : "1",
                         'ir123_Radiance'            : "2",
                         'ir123_Brightness'          : "1",
                         'ir133_Radiance'            : "2",
                         'ir133_Brightness'          : "1",
                         'nr013_Albedo'              : "2",
                         'nr016_Albedo'              : "1",
                         'sw038_Radiance'            : "2",
                         'sw038_Brightness'          : "1",
                         'vi004_Albedo'              : "1",
                         'vi005_Albedo'              : "1",
                         'vi006_Albedo'              : "1",
                         'vi008_Albedo'              : "1",
                         'wv063_Radiance'            : "2",
                         'wv063_Brightness'          : "1",
                         'wv069_Radiance'            : "2",
                         'wv069_Brightness'          : "1",
                         'wv073_Radiance'            : "2",
                         'wv073_Brightness'          : "1",
                         'zenith'                    : "1",
                         'HSR_value'                 : "1",
                         "epoch"                     : "10",
                         "batch_size"                :"1",
                         "es_patience"               :"50",
                         "gan_1"                     :"225",
                         "gan_2"                     :"450",
                         "gan_3"                     :"900",
                         "gan_4"                     :"1800",
                         "dnn_1"                     :"32",
                         "dnn_2"                     :"32",
                         "dnn_3"                     :"16",
                         "optimizer"                 :"1",
                         "loss_func"                 :"1",
                         "learning_rate"             : "0.001",
                         "img_model_test_data_counts":"10" ,
                         "num_model_test_data_ratio" :"0.1",
                         "num_model_pixel_width"     : "3" ,
                         "num_model_pixel_length"    : "3",
                         "use_opt_model"             : "2"}
    #

    tm = making_class( "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance/developing3", info)
    im = making_class( "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance/developing3", info)
    df = {0: np.array(["201910100030"])}
    #train(tm)
    params = init_svc(im)
    using_features = params["using_features"]
    model_path = params["model_path"]
    is_null , reading_ticketNo = fog_data_input_validation.main("201910100030" , using_features, model_path)

    ticketNo_dic = {"reading_ticketNo": reading_ticketNo ,
                        "saving_ticketNo" : "201910100030" }
    rl_generator = params["rl_generator"]
    num_model = params["num_model"]
    using_features = params["using_features"]
    length = params["length"]
    width = params["width"]
    loading_model_dir = params["loading_model_dir"]
    model_path = params["model_path"]

    final_results_array , pred_diff  = fog_preprocess.main(ticketNo_dic ,rl_generator ,num_model , using_features , length , width  , log , error_log)
    postprocess_params , watch_warn_df_params = fog_postprocess.main(ticketNo_dic, final_results_array , log , error_log)
    fog_database.main(postprocess_params , watch_warn_df_params, pred_diff ,log , error_log)


    #inference(df, params, None)
