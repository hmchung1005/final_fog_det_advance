import os
import time
import numpy as np
import pandas as pd
from PIL import Image
from fog_config import gk2a_fog_img_attrs , fog_create_model_info , tb_info , all_basic_path , external_info_path , fog_create_data_path
import jpype
import fog_logging
import jaydebeapi
import tensorflow as tf
from tensorflow import keras
import itertools as it
import scipy.spatial.distance
from sklearn.utils import shuffle
from matplotlib import pyplot as plt
from IPython import display
import random
from datetime import datetime
import json
from random import randint


def set_gpu(gpu_num):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')   # 물리적 GPU리스트
    if physical_devices != []:
        tf.config.experimental.set_visible_devices(physical_devices[gpu_num], 'GPU')   # 3번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.experimental.set_memory_growth(physical_devices[gpu_num], True)



def downsample(filters, size,stride_size = 2 , apply_batchnorm=True):
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(tf.keras.layers.Conv2D(filters, size, strides=stride_size, padding='same',
               kernel_initializer=initializer, use_bias=False))

    if apply_batchnorm:
        result.add(tf.keras.layers.BatchNormalization())

    result.add(tf.keras.layers.LeakyReLU())

    return result

def upsample(filters, size, stride_size = 2, apply_dropout=False):
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(tf.keras.layers.Conv2DTranspose(filters, size, strides=stride_size,
               padding='same',
               kernel_initializer=initializer,
               use_bias=False))

    result.add(tf.keras.layers.BatchNormalization())

    if apply_dropout:
        result.add(tf.keras.layers.Dropout(0.5))

    result.add(tf.keras.layers.ReLU())

    return result


# dbz 값 변환
def dbz_to_r(dBz):
    Z = wrl.trafo.idecibel(dBz)
    R = wrl.zr.z_to_r(Z, a=200, b=1.6)
    R = np.round(R, 5)
    return R


def Generator(total_features_counts , OUTPUT_CHANNELS , node1 = 225 , node2 = 450 ,node3 =900 , node4 = 1800  ):
    inputs = tf.keras.layers.Input(shape=[900,900,total_features_counts])

    down_stack = [
                  downsample(node1, 4, 3, apply_batchnorm=False), # (bs, 128, 128, 64)
                  downsample(node2, 4, 3), # (bs, 64, 64, 128)
                  downsample(node3, 4, 2), # (bs, 32, 32, 256)
                  downsample(node4, 4, 2), # (bs, 16, 16, 512)
                  downsample(node4, 6, 5), # (bs, 8, 8, 512)
                  downsample(node4, 6, 5), # (bs, 4, 4, 512)
                   ]
    up_stack = [
                  upsample(node4, 6, 5, apply_dropout=True), # (bs, 8, 8, 1024)
                  upsample(node4, 4, 5), # (bs, 16, 16, 1024)
                  upsample(node3, 4, 2), # (bs, 32, 32, 512)
                  upsample(node2, 4, 2), # (bs, 64, 64, 256)
                  upsample(node1, 4 ,3), # (bs, 128, 128, 128)
                   ]

    initializer = tf.random_normal_initializer(0., 0.02)
    last = tf.keras.layers.Conv2DTranspose(OUTPUT_CHANNELS , 4,
                                         strides=3,
                                         padding='same',
                                         kernel_initializer=initializer,
                                         activation='tanh') # (bs, 256, 256, 3)
    x = inputs

    # Downsampling through the model
    skips = []
    for down in down_stack:
        x = down(x)
        skips.append(x)

    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, skip in zip(up_stack, skips):
        x = up(x)
        x = tf.keras.layers.Concatenate()([x, skip])

    x = last(x)

    return tf.keras.Model(inputs=inputs, outputs=x)


def generator_loss(disc_generated_output, gen_output, target):
    gan_loss = loss_object(tf.ones_like(disc_generated_output), disc_generated_output)

    # mean absolute error
    l1_loss = tf.reduce_mean(tf.abs(target - gen_output))

    total_gen_loss = gan_loss + (LAMBDA * l1_loss)

    return total_gen_loss, gan_loss, l1_loss




def Discriminator(total_features_counts , OUTPUT_CHANNELS , node1 = 225 , node2 = 450 , node3 = 900 , node4 = 1800):
    initializer = tf.random_normal_initializer(0., 0.02)

    inp = tf.keras.layers.Input(shape=[900, 900, total_features_counts], name='input_image')
    tar = tf.keras.layers.Input(shape=[900, 900, OUTPUT_CHANNELS], name='target_image')

    x = tf.keras.layers.concatenate([inp, tar]) # (bs, 256, 256, channels*2)

    down1 = downsample(node1, 4, 3, False)(x) # (bs, 128, 128, 64)
    down2 = downsample(node2, 4 ,3)(down1) # (bs, 64, 64, 128)
    down3 = downsample(node3, 4 ,2)(down2) # (bs, 32, 32, 256)

    zero_pad1 = tf.keras.layers.ZeroPadding2D()(down3) # (bs, 34, 34, 256)
    conv = tf.keras.layers.Conv2D(node4, 4, strides=1,
                                kernel_initializer=initializer,
                                use_bias=False)(zero_pad1) # (bs, 31, 31, 512)

    batchnorm1 = tf.keras.layers.BatchNormalization()(conv)

    leaky_relu = tf.keras.layers.LeakyReLU()(batchnorm1)

    zero_pad2 = tf.keras.layers.ZeroPadding2D()(leaky_relu) # (bs, 33, 33, 512)

    last = tf.keras.layers.Conv2D(1, 4, strides=1,
                                kernel_initializer=initializer)(zero_pad2) # (bs, 30, 30, 1)

    return tf.keras.Model(inputs=[inp, tar], outputs=last)

def discriminator_loss(disc_real_output, disc_generated_output):
    real_loss = loss_object(tf.ones_like(disc_real_output), disc_real_output)

    generated_loss = loss_object(tf.zeros_like(disc_generated_output), disc_generated_output)

    total_disc_loss = real_loss + generated_loss

    return total_disc_loss



def generate_images(model, test_input, tar):
    prediction = model(test_input, training=True)
    plt.figure(figsize=(15,15))

    display_list = [ tar[0], prediction[0]]
    title = ['Ground Truth', 'Predicted Image']

    for i in range(2):
        plt.subplot(1, 2, i+1)
        plt.title(title[i])
        # getting the pixel values between [0, 1] to plot it.
        plt.imshow(display_list[i] * 0.5 + 0.5)
        plt.axis('off')
    plt.show()


@tf.function(experimental_relax_shapes=True)
def train_step(input_image, target, epoch):
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        gen_output = generator(input_image, training=True)

        disc_real_output = discriminator([input_image, target], training=True)
        disc_generated_output = discriminator([input_image, gen_output], training=True)

        gen_total_loss, gen_gan_loss, gen_l1_loss = generator_loss(disc_generated_output, gen_output, target)

        disc_loss = discriminator_loss(disc_real_output, disc_generated_output)

    generator_gradients = gen_tape.gradient(gen_total_loss,
                                          generator.trainable_variables)
    discriminator_gradients = disc_tape.gradient(disc_loss,
                                               discriminator.trainable_variables)
    generator_optimizer.apply_gradients(zip(generator_gradients,
                                          generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(discriminator_gradients,
                                              discriminator.trainable_variables))

    with summary_writer.as_default():
        tf.summary.scalar('gen_total_loss', gen_total_loss, step=epoch)
        tf.summary.scalar('gen_gan_loss', gen_gan_loss, step=epoch)
        tf.summary.scalar('gen_l1_loss', gen_l1_loss, step=epoch)
        tf.summary.scalar('disc_loss', disc_loss, step=epoch)

@tf.function(experimental_relax_shapes=True)
def last_step(input_image, target, epoch):
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        gen_output = generator(input_image, training=True)

        disc_real_output = discriminator([input_image, target], training=True)
        disc_generated_output = discriminator([input_image, gen_output], training=True)

        gen_total_loss, gen_gan_loss, gen_l1_loss = generator_loss(disc_generated_output, gen_output, target)

        disc_loss = discriminator_loss(disc_real_output, disc_generated_output)

    generator_gradients = gen_tape.gradient(gen_total_loss,
                                          generator.trainable_variables)
    discriminator_gradients = disc_tape.gradient(disc_loss,
                                               discriminator.trainable_variables)
    generator_optimizer.apply_gradients(zip(generator_gradients,
                                          generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(discriminator_gradients,
                                              discriminator.trainable_variables))

    with summary_writer.as_default():
        tf.summary.scalar('gen_total_loss', gen_total_loss, step=epoch)
        tf.summary.scalar('gen_gan_loss', gen_gan_loss, step=epoch)
        tf.summary.scalar('gen_l1_loss', gen_l1_loss, step=epoch)
        tf.summary.scalar('disc_loss', disc_loss, step=epoch)
    return disc_loss ,gen_total_loss, gen_gan_loss, gen_l1_loss




def generate_images_intrain(model, test_input, tar , patience_level , log, train_pic_path, current_epoch):
    prediction = model(test_input, training=True)
    # 조기 종료 조건은 mse 로 확인 한다.
    mse_array = tf.keras.metrics.mean_absolute_error( tar* 255. , prediction * 255. )
    current_mse = tf.reduce_mean(mse_array).numpy()
    log.info("current mse: {}".format(current_mse))

    if current_mse < fog_create_model_info.mse_threshold:
        patience_level = patience_level + 1
    else:
        patience_level = 0
    if current_epoch % fog_create_model_info.saving_train_pic_duration == 0:
        plt.figure(figsize=(15,15))
        display_list = [ tar[0], prediction[0]]
        title = ['Ground Truth', 'Predicted Image']
        for i in range(2):
            plt.subplot(1, 2, i+1)
            plt.title(title[i])
            # getting the pixel values between [0, 1] to plot it.
            plt.imshow(display_list[i] * 0.5 + 0.5)
            plt.axis('off')
        train_pic_file_path = os.path.join(train_pic_path ,'test_pic_epoch_{}.png'.format(current_epoch) )
        plt.savefig(train_pic_file_path)
    return patience_level




def fit(train_ds, epochs, test_ds, log, error_log , es_patience , saving_img_path):
    mse_threshold = fog_create_model_info.mse_threshold
    patience_level = 0
    last_n = 0
    try:
        test_list = list(test_ds)
        for epoch in range(epochs):
            current_epoch = epoch
            if patience_level > es_patience:
                log.info("es_patience reached over {} ----> early stopping training".format(es_patience))
                break

            epoch = tf.convert_to_tensor(epoch, dtype=tf.int64)

            start = time.time()

            display.clear_output(wait=True)

            #for example_input, example_target in test_ds.take(1):
            test_index = randint(0,int(len(test_list) -1 ) )
            example_input = test_list[test_index][0]
            example_target = test_list[test_index][1]

            # 학습 도중 정해진 epoch 마다 추론 결과 이미지 생성
            # 조기 종료 조건 확인
            patience_level = generate_images_intrain(generator,
                                                     example_input,
                                                     example_target,
                                                     patience_level,
                                                     log,
                                                     saving_img_path,
                                                     current_epoch)
                #generate_images(generator , example_input , example_target)
            log.info("Epoch: {}".format(epoch))
            log.info("patiene_level: {}".format(patience_level))

            # Train
            for n , (input_image , target) in train_ds.enumerate():
                print('.', end='')
                if epoch == 0:
                    last_n = last_n + 1
                    log.info("first epoch - current step : {}".format(last_n))
                else:
                    log.info("current_step : {}".format(n))

                if (n+1) % 100 == 0:
                    print()
                if n == (last_n-1) and epoch != 0 :
                    disc_loss ,gen_total_loss, gen_gan_loss, gen_l1_loss = last_step(input_image, target, epoch)
                    disc_loss ,gen_total_loss, gen_gan_loss, gen_l1_loss
                    log.info("discriminator loss: {}".format(disc_loss.numpy()))
                    log.info("generator total loss: {}".format(gen_total_loss.numpy()))
                    log.info("generator gan loss: {}".format(gen_gan_loss.numpy()))
                    log.info("generator l1 loss: {}".format(gen_l1_loss.numpy()))
                else:
                    train_step(input_image, target, epoch)

            # saving (checkpoint) the model every fixed number of epochs
            # checkpoint_interval 마다 학습 모델 체크 포인트 저장
            if (epoch + 1) % fog_create_model_info.checkpoint_interval == 0:
                checkpoint.save(file_prefix = checkpoint_prefix)
            print()
            log.info('Time taken for epoch {} is {} sec\n'.format(str(current_epoch + 1),str(time.time()-start)) )
        checkpoint.save(file_prefix = checkpoint_prefix)
        return last_n
    except Exception as e:
        error_log.error("error occured during training --. {}".format(e))



# 이미지 모델을 위한 파싱. 입력 데이터 x 와 라벨링 데이터 y 필요
def _parse_function(example_proto):
    keys_to_features = {'X':tf.io.FixedLenFeature(900 * 900 * total_features_counts, tf.float32),
                        'Y':tf.io.FixedLenFeature(900 * 900 * 3, tf.float32)}
    parsed_features = tf.io.parse_single_example(example_proto, keys_to_features)
    reshaped_x = tf.reshape(parsed_features["X"], (900 , 900 , total_features_counts) )
    reshaped_y = tf.reshape(parsed_features["Y"] , (900,900,3))
    return reshaped_x, reshaped_y

# 수치 모델을 위한 파싱. 입력 데이터 x 와 학습 날짜 데이터 z 필요
def _parse_function2(example_proto):
    keys_to_features = {'X':tf.io.FixedLenFeature(900 * 900 * total_features_counts, tf.float32),
                        'Z': tf.io.FixedLenFeature([], tf.int64, default_value=0)}
    parsed_features = tf.io.parse_single_example(example_proto, keys_to_features)
    reshaped_x = tf.reshape(parsed_features["X"], (900 , 900 , total_features_counts) )

    return reshaped_x, parsed_features["Z"]



class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.int64):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def open_connection(host,db_name,user_name,password, port ,root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor

# 수치 모델 데이터 생성
def make_num_model_data(dataset , model, length, width,  train_start_ticket , train_end_ticket,  log , error_log):

    from datetime import date, datetime, timedelta
    train_start_ticket_kr = datetime.strptime(train_start_ticket, '%Y%m%d%H%M') + timedelta(minutes=540)
    train_start_ticket = train_start_ticket_kr.strftime('%Y%m%d%H%M')

    train_end_ticket_kr = datetime.strptime(train_end_ticket, '%Y%m%d%H%M') + timedelta(minutes=540)
    train_end_ticket = train_end_ticket_kr.strftime('%Y%m%d%H%M')




    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port
    conn, cursor = open_connection(host,db_name,user_name,password,port , all_basic_path.root_source_path)

    pixel_info = pd.read_csv(external_info_path.training_amos_info_path)[["tower", "lat_index_900" , "lon_index_900"]]
    # 기지 중심 픽셀로부터 왼쪽 가장자리 픽셀 위치
    pixel_info["left_end"] = pixel_info["lon_index_900"] - width
    # 기지 중심 픽셀로 부터 오른쪽 가장자리 픽셀 위치
    pixel_info["right_end"] = pixel_info["lon_index_900"] + (width + 1)
    # 기지 중심 픽셀로 부터 위에 가장자리 픽셀 위치
    pixel_info["up_end"] = pixel_info["lat_index_900"] - length
    # 기지 중심 픽셀로 부터 아래 가장자리 픽셀 위치
    pixel_info["down_end"] = pixel_info["lat_index_900"] + (length + 1)

    pixel_info["tower"] = pixel_info["tower"].astype("str")
    pixel_info  = pixel_info.set_index("tower")
    left_end_dic = pixel_info["left_end"].to_dict()
    right_end_dic = pixel_info["right_end"].to_dict()
    up_end_dic = pixel_info["up_end"].to_dict()
    down_end_dic = pixel_info["down_end"].to_dict()
    num_model_data_dic = {}
    # for i , (input, ticket) in dataset.take(1).enumerate():
    #     print()

    # amos 시정 거리 데이터 읽어 오기
    sql = "select K1 , K2 , SU7 from D_AMOS_DETAIL where K1 >= '{}' and k1 <= '{}'  and K2 != '153' and k2 != '992';".format(train_start_ticket , train_end_ticket)
    cursor.execute(sql)
    data = cursor.fetchall()

    sql_153 = "select K1 , K2 , SU8 from D_AMOS_DETAIL where K1 >= '{}' and k1 <= '{}' and K2 = '153';".format(train_start_ticket , train_end_ticket)
    cursor.execute(sql_153)
    data_153 = cursor.fetchall()
    whole_data = data + data_153

    for i ,(input, ticket) in dataset.enumerate():
        log.info("predicting {}th picture".format(i))
        # 이미지 모델 추론값 생성 ----> 이미지 모델의 추론값이 수치 모델에서는 입력 값이다
        prediction = model(input, training=True)

        prediction_np = prediction.numpy()[0]
        prediction_np = prediction_np.astype("float32")
        ticketNo = ticket.numpy()[0]

        ticketNo_kr = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
        ticketNo = ticketNo_kr.strftime('%Y%m%d%H%M')

        # 이미지 모델의 날짜 데이터와 매핑 하는 모든 기지 데이터 필터
        current_data = list(filter(lambda x: x[0] == str(ticketNo) , whole_data  ))
        # 데이터에서 각 기지 번호 리스트
        target_base_list = [x[1] for x in current_data]
        # 데이터에서 각 시정거리 정보 리스트
        target_list = [x[2] for x in current_data]

        for target_base , target in zip(target_base_list , target_list):
            try:
                if float(target) > 0 :
                    pixel_v = prediction_np[up_end_dic[target_base] : down_end_dic[target_base] , left_end_dic[target_base] : right_end_dic[target_base]  ]
                    # 수치 모델 데이터 ---> 딕셔너리
                    # 딕셔너리의 키 값이 y값이다. 키 값은 다음과 같은 형태 ---->   "날짜_기지번호_시정거리"
                    # 딕셔너리의 value 값이 x 값으로 이미지 모델이 추론한 rgb 값이다.
                    num_model_data_dic["{}_{}_{}".format(ticketNo , target_base , target)] = pixel_v
            except Exception as e:
                error_log.error("error making num_model data : {}".format(e))


    conn.close()
    cursor.close()
    return num_model_data_dic








def main(train_data_params  , img_model_dir , training_data_dir, log , error_log):

    BUFFER_SIZE = 400
    # 배치 사이즈는 1을 설정해야 메모리에 문제가 안간다
    BATCH_SIZE =  train_data_params["batch_size_num"]
    EPOCHS = train_data_params["epoch_num"]
    # 갠 모델 네트워크 노드 개수
    gan_1=int(train_data_params["gan_1"])
    gan_2=int(train_data_params["gan_2"])
    gan_3=int(train_data_params["gan_3"])
    gan_4=int(train_data_params["gan_4"])

    # 모델 조기 종료 조건
    es_patience = train_data_params["es_patience"]



    IMG_WIDTH = 900
    IMG_HEIGHT = 900

    OUTPUT_CHANNELS = 3

    # 학습 데이터 feature
    using_features = train_data_params["using_features"]

    # feature 개수
    global total_features_counts
    total_features_counts =len(using_features)

    global LAMBDA
    LAMBDA = fog_create_model_info.LAMBDA

    # 생성자 모델
    global generator
    generator = Generator(total_features_counts=total_features_counts,
                          OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                          node1 = gan_1,
                          node2 = gan_2,
                          node3 = gan_3,
                          node4 = gan_4)

    # 판별자 모델
    global discriminator
    discriminator = Discriminator(total_features_counts=total_features_counts,
                          OUTPUT_CHANNELS = OUTPUT_CHANNELS,
                          node1 = gan_1,
                          node2 = gan_2,
                          node3 = gan_3,
                          node4 = gan_4)
    # 이미지 모델에서는 로스 함수, 옵티마이저 수정 불가
    global loss_object
    loss_object = tf.keras.losses.BinaryCrossentropy(from_logits=True)

    global generator_optimizer
    generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

    global discriminator_optimizer
    discriminator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)


    # 체크 포인트 저장 경로
    checkpoint_dir = os.path.join(img_model_dir , 'training_checkpoints')
    global checkpoint_prefix
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    global checkpoint
    checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,discriminator_optimizer=discriminator_optimizer,generator=generator,discriminator=discriminator)

    # generate_images(checkpoint.generator, normalized_x_np[0][tf.newaxis , ...], final_y_np[0][tf.newaxis , ...])

    log_dir = os.path.join(img_model_dir,   "logs/")
    global summary_writer
    summary_writer = tf.summary.create_file_writer(log_dir + "fit/" + datetime.now().strftime("%Y%m%d-%H%M%S"))

    # 실험 데이터 개수
    test_data_counts = train_data_params["img_model_test_data_counts"]

    filenames = os.listdir(training_data_dir)
    filenames = list(map(lambda x: os.path.join(training_data_dir , x) , filenames ))
    filenames = list(filter(lambda x: x.endswith("tfrecord") , filenames))


    # 이미지 모델 학습 데이터 tensorflow api 연동
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(_parse_function ,num_parallel_calls= int(((len(filenames) +1) / 2)))  # 현재 병렬 로직 : 전체 파일 개수 / 2 ---> 속도 실험 필요
    dataset = dataset.batch(BATCH_SIZE)
    # 셔플 하는 순간 심각히 느려짐
    # dataset = tf.data.Dataset.from_tensor_slices(filenames)
    # dataset = dataset.interleave(lambda x: tf.data.TFRecordDataset(x).map(_parse_function),
    #     cycle_length=4, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    # dataset = dataset.batch(BATCH_SIZE)

    # 실험 데이터 분리
    test_dataset = dataset.take(test_data_counts)
    train_dataset = dataset.skip(test_data_counts)

    train_pic_path = os.path.join(img_model_dir , "train_pic")
    if not os.path.exists(train_pic_path):
        os.makedirs(train_pic_path)

    # last_n 학습 데이터 개수
    last_n = fit(train_dataset, EPOCHS, test_dataset , log , error_log, es_patience ,train_pic_path)



    # 수치 모델 학습 데이터 tensorflow api 연동
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(_parse_function2 ,num_parallel_calls= int(((len(filenames) +1) / 2)))
    dataset = dataset.batch(BATCH_SIZE)

    train_start_ticket = train_data_params["train_start_ticket"]
    train_end_ticket = train_data_params["train_end_ticket"]

    # 수치 모델  y축 픽셀 길이
    length = train_data_params["num_model_pixel_length"]
    # 이미지 모델 x축 픽셀 길이
    width = train_data_params["num_model_pixel_width"]

    # 수치 모델 데이터 생성
    num_model_data_dic = make_num_model_data(dataset , generator, length, width,  train_start_ticket , train_end_ticket, log , error_log)


    #flag
    ######## 셍상힌 수치 모델 데이터를 저장 하고 싶으면 이 코드 적용  ############################################
    # num_model_data_path = os.path.join(fog_create_data_path.create_num_data_path , "num_model_data_dic.json")
    # with open(num_model_data_path , "w") as fp:
    #     json.dump(num_model_data_dic  , fp ,cls=NumpyEncoder)
    # fp.close()
    ##########################################################################################################

    #flag
    ########  생성한 이미지 모델 데이터를 삭제 하고 싶으면 이 코드 적용 ##########################################################
    # previous_data = os.listdir(training_data_dir)
    # if len(previous_data) > 0:
    #     previous_data_path = list(map(lambda x: os.path.join(training_data_dir , x) , previous_data ))
    #     for f in previous_data_path:
    #         os.remove(f)
    ############################################################################################

    # 생성한 모델을 최적 모델로 사용 하게 되면 다음 json 파일 필요
    gan_node_dic_path = os.path.join(img_model_dir , "gan_node_dic.json")
    gan_node_dic = {"1" :gan_1,
                    "2" :gan_2,
                    "3" :gan_3,
                    "4" :gan_4}

    with open(gan_node_dic_path , "w") as fp:
        json.dump(gan_node_dic  , fp ,cls=NumpyEncoder)
    fp.close()

    return num_model_data_dic



if __name__ == '__main__':

    set_gpu(1)

    train_data_params = {"train_start_ticket":"201910010000",
                         "train_end_ticket"  :"201910010030",
                         "using_features":  ['ir087_Radiance',
                                             #'ir087_Brightness',
                                             'ir096_Radiance',
                                             #'ir096_Brightness',
                                             'ir105_Radiance',
                                             #'ir105_Brightness',
                                             'ir112_Radiance',
                                             #'ir112_Brightness',
                                             'ir123_Radiance',
                                             #'ir123_Brightness',
                                             'ir133_Radiance',
                                             #'ir133_Brightness',
                                             'nr013_Albedo',
                                             'nr016_Albedo',
                                             'sw038_Radiance',
                                             #'sw038_Brightness',
                                             'vi004_Albedo',
                                             'vi005_Albedo',
                                             'vi006_Albedo',
                                             'vi008_Albedo',
                                             'wv063_Radiance',
                                             #'wv063_Brightness',
                                             'wv069_Radiance',
                                             #'wv069_Brightness',
                                             'wv073_Radiance',
                                             #'wv073_Brightness',
                                             'zenith'],
                                             #'HSR_value'],
                         "model_path": "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance/developing2",
                         "epoch_num" : 110,
                         "batch_size_num":1,
                         "es_patience" : 50,
                         "gan_1":225,
                         "gan_2":450,
                         "gan_3":900,
                         "gan_4":1800,
                         "dnn_1":32,
                         "dnn_2":32,
                         "dnn_3":16,
                         "optimizer":"1",
                         "loss_func":"1",
                         "learning_rate":0.001,
                         "img_model_test_data_counts":10 ,
                         "num_model_test_data_ratio" :0.1,
                         "num_model_pixel_width" : 3 ,
                         "num_model_pixel_length" : 3}

    log = fog_logging.get_log_view(1, "platform", False, 'developing_log2')
    error_log = fog_logging.get_log_view(1, "platform", True, 'developing_errorlog2')
    saving_model_path = train_data_params["model_path"]
    img_model_dir = "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance/developing/image_model"

    training_data_dir = "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance/developing/image_model/training_data"


    num_model_data_dic = main(train_data_params ,img_model_dir , training_data_dir , log , error_log )
    print("done")
