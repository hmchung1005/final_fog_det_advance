
import os
import time
import csv
import numpy as np
import pandas as pd
import requests
import json
import multiprocessing as mp
import fog_logging
from datetime import date, datetime, timedelta , timezone
from hdfs import InsecureClient
import math

from pysolar import solar
from PIL import Image
from fog_config import hdfs_path_class, fog_create_data_path , gk2a_fog_img_attrs , preprocess_info , amos_db_feature_naming


def getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user):
    active_host = None

    #  ambari user/pass
    #auth_user = 'admin'

    #auth_pass = 'admin'
    #ambari_host = "192.168.0.174"
    #  ambari host
    #ambari_cluster_name = 'hacluster'


    api_url = 'http://{}:8080/api/v1/clusters/{}/host_components?HostRoles/component_name=NAMENODE&metrics/dfs/FSNamesystem/HAState=active'.format(ambari_host,
                                                                                                                                                   ambari_cluster_name)
    res = requests.get(api_url, auth=(auth_user, auth_pass))

    status_code = res.status_code
    res_json = res.json()

    # - status 200 == 정상
    # - status 403 == 암바리 계정명 불일치
    # - status 404 == api url 재확인 필요
    # - status 5xx == 암바리 서버 에러

    if status_code==200 and res_json['items']:
        active_host = res_json['items'][0]['HostRoles']['host_name']

    return status_code, active_host



def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta

def normalize_whole_array(x_data , using_features , features_min_max_dic):
    for i , feature in enumerate(using_features):
        temp_min = features_min_max_dic[feature][0]
        temp_max = features_min_max_dic[feature][1]
        x_data[:,:,:,i] = (x_data[:,:,:,i] - temp_min) / (temp_max - temp_min)
    return x_data
def normalize_one_array(x_data , using_features , features_min_max_dic):
    for i , feature in enumerate(using_features):
        temp_min = features_min_max_dic[feature][0]
        temp_max = features_min_max_dic[feature][1]
        x_data[:,:,i] = (x_data[:,:,i] - temp_min) / (temp_max - temp_min)
    return x_data



def read_gk2a_fog_img(ticket, dif_rgba_list , log, error_log):

    fog_img_path = fog_create_data_path.gk2a_fog_img_path.format(ticket[:6] , ticket)
    raw_image = Image.open(fog_img_path)
    raw_array = np.array(raw_image)
    raw_array = raw_array[:,:,:3]
    new_array = np.full((810000,3) , gk2a_fog_img_attrs.no_diff_rgba  , dtype = "uint8")
    flattend_raw_array = np.reshape(raw_array , (810000 , 3))

    for diff_rgba in dif_rgba_list:

        diff_masked_array = (flattend_raw_array[:,0] == diff_rgba[0] ) & (flattend_raw_array[:,1] ==diff_rgba[1] ) & (flattend_raw_array[:,2] ==diff_rgba[2] )

        new_array[diff_masked_array] = diff_rgba[:3]
    result_array = np.reshape(new_array ,  (900,900, 3))

    result_array[22:50 , :180 , :] = [gk2a_fog_img_attrs.no_diff_rgba] * 3
    result_array[855 : , 525:,:] = [gk2a_fog_img_attrs.no_diff_rgba] * 3

    final_array = result_array / 255.
    final_array = final_array.astype("float32")
    log.info("sucess loading fog_img : {}".format(ticket))
    return final_array


def whole_single_process_x_data(gk2a_timelist, using_features , training_data_dir):

    log = fog_logging.get_log_view(1, "platform", False, 'critical_info_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'critical_info_error_log')

    dif_rgba_list = [gk2a_fog_img_attrs.diff1_rgba,
                      gk2a_fog_img_attrs.diff2_rgba,
                      gk2a_fog_img_attrs.diff3_rgba,
                      gk2a_fog_img_attrs.diff4_rgba,
                      gk2a_fog_img_attrs.diff5_rgba,
                      gk2a_fog_img_attrs.diff6_rgba]


    features_min_max_dic = preprocess_info.features_min_max

    hdfs_nas  = preprocess_info.hdfs_nas

    hsr_mapping_table = pd.read_csv(fog_create_data_path.hsr_mapping_table)
    hsr_merge_table = hsr_mapping_table.loc[(hsr_mapping_table['hsr_y'] >= 0 ) & (hsr_mapping_table['hsr_y'] <= 2880) & (hsr_mapping_table['hsr_x'] >= 0 ) & (hsr_mapping_table['hsr_x'] <= 2304 ),: ]

    latlon_900_900 = pd.read_csv(fog_create_data_path.latlon_900_900,header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)

    step_size = preprocess_info.step_size
    total_steps = math.ceil(len(gk2a_timelist) /  step_size  )


    for each_step in range(1,total_steps + 1):
        try:
            whole_keys     = ()
            whole_x_values = ()
            whole_y_values = ()

            temp_gk2a_timelist=  gk2a_timelist[(each_step - 1) * step_size : each_step * step_size ]
            res_lst = [[x , using_features, hsr_merge_table , latlon_900_2d , dif_rgba_list] for x in temp_gk2a_timelist]
            for i in range(len(res_lst)):
                if hdfs_nas =="nas":
                    cr = read_x_data_nas(res_lst[i])
                else:
                    cr = read_x_data_hdfs(res_lst[i])
                if cr != None:
                    key      = (cr[0] , )
                    x_value  = (cr[1] , )
                    y_value  = (cr[2] , )

                    whole_keys = whole_keys + key
                    whole_x_values = whole_x_values + x_value
                    whole_y_values = whole_y_values + y_value

            x_values_array = np.asarray(whole_x_values)
            y_values_array = np.asarray(whole_y_values)
            keys_array = np.asarray(whole_keys)

            normalized_x_values_array = normalize_whole_array(x_values_array , using_features , features_min_max_dic)

            ###### if you dont want to save large datset, comment this out #######
            ######################################################################
            log.info("iteration {} data saving".format(each_step))
            data_path = os.path.join(training_data_dir , "data_{}".format(each_step))
            np.savez_compressed(data_path , tickets =keys_array, x_values = normalized_x_values_array , y_values = y_values_array   )
            log.info("saved success")
            ######################################################################
        except Exception as e:
            error_log.error("whole_multi_process_x_data error at {} ----> {}".format(each_step , e))
            continue
    return whole_dataset , total_steps

# 태양 고도 빠르게 계산 함수
def get_altitude_faster(latlon_900_2d, when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(latlon_900_2d[:,0])
    hour_angle = solar.get_hour_angle(when, latlon_900_2d[:,1] )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))



# 태양 천정각 빠르게 계산
def create_zenith_array(ticketNo , latlon_900_2d , log , error_log ):
    # try:
    kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
    year = int(kr_ticketNo[:4])
    month = int(kr_ticketNo[4:6])
    day = int(kr_ticketNo[6:8])
    hour = int(kr_ticketNo[8:10])
    minute = int(kr_ticketNo[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime(year,month,day,hour,minute, tzinfo =timezone.utc)  - timedelta(hours = 9)
    temp_array = get_altitude_faster(latlon_900_2d, dobj)
    # 900 900 2 차원 배열로 만듬
    whole_array = np.reshape(temp_array, (900,900) )
    final_array = float(90) - whole_array
    log.info("zenith created")
    return final_array


def read_hsr_array_nas(ticketNo , hsr_merge_table, log ,error_log):
    # try:
    kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
    kr_yearmonth = kr_ticketNo[:6]
    kr_day = kr_ticketNo[6:8]
    file_path = fog_create_data_path.hsr_array_nas_path.format(kr_yearmonth , kr_day , kr_ticketNo, kr_ticketNo)
    temp_array = pd.read_csv(file_path , header = None ).iloc[:,:-1].values
    hsr_y_list = hsr_merge_table["hsr_y"].values
    hsr_x_list = hsr_merge_table["hsr_x"].values
    gk2a_y_list = hsr_merge_table["gk2a_y"].values
    gk2a_x_list = hsr_merge_table["gk2a_x"].values
    returning_array = np.full((900,900),-200)
    inserting_values = temp_array[hsr_y_list , hsr_x_list]
    returning_array[gk2a_y_list, gk2a_x_list] = inserting_values
    rain_np = dbz_to_r(returning_array)
    log.info("HSR_Value loaded")
    return rain_np

def read_hsr_array_hdfs(ticketNo , hsr_merge_table, log ,error_log):
    # try:
    kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
    kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
    kr_yearmonth = kr_ticketNo[:6]
    kr_day = kr_ticketNo[6:8]

    ambari_host = hdfs_path_class.ambari_host
    auth_pass = hdfs_path_class.auth_pass
    ambari_cluster_name = hdfs_path_class.ambari_cluster_name
    auth_user = hdfs_path_class.auth_user

    hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass ,ambari_cluster_name ,auth_user)

    if hdfs_status != 200:
        log.info("hdfs status : {}".format(hdfs_status))

    working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)

    file_path = fog_create_data_path.hsr_array_hdfs_path.format(kr_yearmonth , kr_day , kr_ticketNo)

    client = InsecureClient(working_hdfs_path, user="hdfs")
    status = client.status(file_path,  strict = False)
    if status  == None:
        log.info("no file found at : {}".format(file_path))

    with client.read(file_path) as file:
        temp_array = pd.read_csv(file, header = None ).iloc[:,:-1].values

    hsr_y_list = hsr_merge_table["hsr_y"].values
    hsr_x_list = hsr_merge_table["hsr_x"].values
    gk2a_y_list = hsr_merge_table["gk2a_y"].values
    gk2a_x_list = hsr_merge_table["gk2a_x"].values
    returning_array = np.full((900,900),-200)
    inserting_values = temp_array[hsr_y_list , hsr_x_list]
    returning_array[gk2a_y_list, gk2a_x_list] = inserting_values
    rain_np = dbz_to_r(returning_array)
    log.info("HSR_Value loaded")
    return rain_np



def dbz_to_r(dBz):
    Z = idecibel(dBz)
    R = z_to_r(Z, a=200, b=1.6)
    R = np.round(R, 5)
    return R

def idecibel(x):

    return 10.0 ** (x / 10.0)


def z_to_r(z, a=200.0, b=1.6):

    return (z / a) ** (1.0 / b)






# yearmonth #day #hour #size #ticket
def read_gk2a_x_array_nas(ticketNo , feature, log ,error_log):
    # try:
    yearmonth = ticketNo[:6]
    day = ticketNo[6:8]
    hour = ticketNo[8:10]
    channel = feature.split("_")[0]
    if "vi006" in feature:
        size = "05"
    elif "vi" in feature:
        size = "10"
    else:
        size = "20"
    folder_path = fog_create_data_path.gk2a_array_nas_folder.format(yearmonth , day , hour, channel ,size , ticketNo)
    file_list = os.listdir(folder_path)
    key_word1 =  "nc." + feature.split("_")[1]
    key_word2 =  "nc" + feature.split("_")[1]
    available_file = list(filter(lambda x: key_word1 in x or key_word2 in x , file_list ))[0]
    final_file_path = os.path.join(folder_path , available_file)
    if final_file_path.endswith("npz"):
        temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
    else:
        temp = pd.read_csv(final_file_path ,header = None ,chunksize = 1000)
        temp_df = temp.read()
        log.info("using chunksize")
        temp_array = temp_df.values

    wide_len = temp_array.shape[1]
    if wide_len == 3601 or wide_len == 1801 or wide_len == 901:
        log.info("extra line at the array ----> deleting last line")
        temp_array = temp_array[: , : -1]
    if temp_array[0 , -1] == " " or temp_array[1 , -1] == " ":
        log.info("parsing error ---> shifting values to right by 1 pixel")
        temp_array[: ,1:] = temp_array[:, :-1]
    try:
        temp_array = temp_array.astype("float32")
    except Exception as e:
        log.info("error at type as float : {}".format(e))
        y_length = temp_array.shape[0]
        x_length = temp_array.shape[1]
        temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
        temp_array = np.reshape(temp_array  , (y_length , x_length) )
        temp_array = temp_array.astype("float")
        log.info("success making it numeric")

    if "vi006" in feature:
        try:
            temp_array = transform_gk_3600_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_3600_to_900 ----> {}".format(e))
    elif "vi" in feature:
        try:
            temp_array = transform_gk_1800_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_1800_to_900 ----> {}".format(e))

    log.info("success loading {} : {}".format(ticketNo , feature))

    if np.isnan(np.sum(temp_array)):
        log.info("nan value found in the array ---> masking array")
        mask = np.isnan(temp_array)
        temp_array[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), temp_array[~mask])
        log.info('masking succsss')
    return temp_array



def read_gk2a_x_array_hdfs(ticketNo , feature, log ,error_log):
    yearmonth = ticketNo[:6]
    day = ticketNo[6:8]

    channel , feature_type = feature.split("_")

    ambari_host = hdfs_path_class.ambari_host
    auth_pass = hdfs_path_class.auth_pass
    ambari_cluster_name = hdfs_path_class.ambari_cluster_name
    auth_user = hdfs_path_class.auth_user
    hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host, auth_pass , ambari_cluster_name,auth_user )
    if hdfs_status  !=  200:
        log.info("hdfs status : {}".format(hdfs_status))

    if "vi006" in feature:
        size = "05"
    elif "vi" in feature:
        size = "10"
    else:
        size = "20"
    if feature_type == "Brightness" : feature_type = "Brightness_Temperature"


    file_path = fog_create_data_path.gk2a_array_hdfs_path.format(channel.upper() ,yearmonth , day ,  channel , size , ticketNo , feature_type)
    #capital channel #yearmonth  #day #channel #size #ticketNo  #feature (Brightness_Temperature)

    working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)

    client = InsecureClient(working_hdfs_path, user="hdfs")
    file_status = client.status(file_path,  strict = False)
    if file_status  == None:
        log.info("no file found at : {}".format(file_path))
    with client.read(file_path) as file:
        temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values

    wide_len = temp_array.shape[1]
    if wide_len == 3601 or wide_len == 1801 or wide_len == 901:
        log.info("extra line at the array ----> deleting last line")
        temp_array = temp_array[: , : -1]
    if temp_array[0 , -1] == " " or temp_array[1 , -1] == " ":
        log.info("parsing error ---> shifting values to right by 1 pixel")
        temp_array[: ,1:] = temp_array[:, :-1]
    try:
        temp_array = temp_array.astype("float")
    except Exception as e:
        log.info("error at type as float : {}".format(e))
        y_length = temp_array.shape[0]
        x_length = temp_array.shape[1]
        temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
        temp_array = np.reshape(temp_array  , (y_length , x_length) )
        temp_array = temp_array.astype("float")
        log.info("success making it numeric")
    if "vi006" in feature:
        try:
            temp_array = transform_gk_3600_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_3600_to_900 ----> {}".format(e))
    elif "vi" in feature:
        try:
            temp_array = transform_gk_1800_to_900(temp_array)
        except Exception as e:
            error_log.error("error transform_gk_1800_to_900 ----> {}".format(e))
    log.info("success loading {} : {}".format(ticketNo , feature))
    if np.isnan(np.sum(temp_array)):
        log.info("nan value found in the array ---> masking array")
        mask = np.isnan(temp_array)
        temp_array[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), temp_array[~mask])
        log.info('masking succsss')
    return temp_array



def transform_gk_3600_to_900(array_3600):
    array_list = []
    for i in range(4):
        for j in range(4):
            temp_array = array_3600[i::4 ,j::4]
            array_list.append(temp_array)

    final_array = sum(array_list) / 16
    return final_array

def transform_gk_1800_to_900(array_1800):
    a = array_1800[::2, ::2]
    b = array_1800[::2, 1::2]
    c = array_1800[1::2, ::2]
    d = array_1800[1::2, 1::2]
    sum = a +b+ c+d
    result_array = sum / 4
    return result_array



## last flag starts
def whole_multi_process_x_data(gk2a_timelist , using_features, training_data_dir):
    #os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    dif_rgba_list = [gk2a_fog_img_attrs.diff1_rgba,
                      gk2a_fog_img_attrs.diff2_rgba,
                      gk2a_fog_img_attrs.diff3_rgba,
                      gk2a_fog_img_attrs.diff4_rgba,
                      gk2a_fog_img_attrs.diff5_rgba,
                      gk2a_fog_img_attrs.diff6_rgba]

    cpu_count = preprocess_info.create_data_cpu_count
    hdfs_nas  = preprocess_info.hdfs_nas

    hsr_mapping_table = pd.read_csv(fog_create_data_path.hsr_mapping_table)
    hsr_merge_table = hsr_mapping_table.loc[(hsr_mapping_table['hsr_y'] >= 0 ) & (hsr_mapping_table['hsr_y'] <= 2880) & (hsr_mapping_table['hsr_x'] >= 0 ) & (hsr_mapping_table['hsr_x'] <= 2304 ),: ]


    latlon_900_900 = pd.read_csv(fog_create_data_path.latlon_900_900,header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)

    mp.set_start_method('forkserver', force=True)
    pool = mp.Pool(cpu_count)

    step_size = preprocess_info.step_size

    time_chunks = [gk2a_timelist[x:x+step_size] for x in range(0, len(gk2a_timelist), step_size)]
    res_lst = [[i , x , using_features, hsr_merge_table , latlon_900_2d , dif_rgba_list, training_data_dir] for i , x in enumerate(time_chunks)]

    dfl = pool.map(read_x_data, res_lst)

    pool.close()
    pool.join()


# tf record 형태로 파일 저장
def read_x_data(arg_list):
    i , gk2a_timelist , using_features, hsr_merge_table , latlon_900_2d , dif_rgba_list , training_data_dir = arg_list
    hdfs_nas  = preprocess_info.hdfs_nas
    log = fog_logging.get_log_view(1, "platform", False, 'critical_info_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'critical_info_error_log')

    tf_file_path = os.path.join(training_data_dir , "data_{}.tfrecord".format(i))

    import tensorflow as tf
    writer = tf.io.TFRecordWriter(tf_file_path)

    if hdfs_nas == "nas":
        read_x_data_each = read_x_data_nas
    else:
        read_x_data_each = read_x_data_hdfs
    features_min_max_dic = preprocess_info.features_min_max

    for ticketNo in gk2a_timelist:
        result = read_x_data_each(ticketNo , using_features ,hsr_merge_table ,latlon_900_2d , dif_rgba_list , log , error_log, features_min_max_dic)

        if result == None:
            log.info("None value found")
            continue

        Z,X,Y = result
        feature = {}
        feature['X'] = tf.train.Feature(float_list=tf.train.FloatList(value=X.flatten() ) )
        feature['Y'] = tf.train.Feature(float_list=tf.train.FloatList(value=Y.flatten() ) )
        feature["Z"] = tf.train.Feature(int64_list=tf.train.Int64List(value=[Z]))
        example = tf.train.Example(features=tf.train.Features(feature=feature))
        serialized = example.SerializeToString()
        writer.write(serialized)
    writer.close()




#@@@
def read_x_data_nas(ticketNo , using_features ,hsr_merge_table ,latlon_900_2d , dif_rgba_list , log , error_log , features_min_max_dic):
    x_data = np.zeros((900, 900 , len(using_features)), dtype ="float32" )
    try:
        for order ,feature in enumerate(using_features):
            log.info(feature)
            if feature == "HSR_value":
                temp_array = read_hsr_array_nas(ticketNo , hsr_merge_table , log , error_log)
            elif feature == "zenith":
                temp_array = create_zenith_array(ticketNo , latlon_900_2d , log , error_log)
            else:
                temp_array = read_gk2a_x_array_nas(ticketNo , feature , log , error_log)
            x_data[: , : , order] = temp_array
        y_data = read_gk2a_fog_img(ticketNo, dif_rgba_list , log, error_log)
        x_data = normalize_one_array(x_data , using_features , features_min_max_dic)
        return (int(ticketNo) ,x_data , y_data)
    except Exception as e:
        error_log.error("error reading ticket at : {} ----> {}".format(ticketNo , e))


def read_x_data_hdfs(ticketNo , using_features ,hsr_merge_table ,latlon_900_2d , dif_rgba_list , log , error_log ,features_min_max_dic):

    x_data = np.zeros((900, 900 , len(using_features))  )
    try:
        for order , feature in enumerate(using_features):
            log.info(feature)
            if feature == "HSR_value":
                temp_array = read_hsr_array_hdfs(ticketNo , hsr_merge_table , log , error_log)
            elif feature == "zenith":
                temp_array = create_zenith_array(ticketNo , latlon_900_2d , log , error_log)
            else:
                temp_array = read_gk2a_x_array_hdfs(ticketNo , feature , log , error_log)
            x_data[:, : , order ] = temp_array
        y_data = read_gk2a_fog_img(ticketNo, dif_rgba_list , log, error_log)
        x_data = normalize_one_array(x_data , using_features , features_min_max_dic)
        return (int(ticketNo) , x_data , y_data)
    except Exception as e:
        error_log.error("error reading ticket at : {} --->{}".format(ticketNo , e))


def main(train_data_params , log ,error_log):
    # 학습 시작 날짜
    train_start_ticket = int(train_data_params["train_start_ticket"])
    # 학습 종류 날짜
    train_end_ticket   = int(train_data_params["train_end_ticket"])
    # 사용 feature
    using_features = train_data_params["using_features"]
    # 모델 저장 경로
    saving_model_path = train_data_params["model_path"]

    fmt = '%Y%m%d%H%M'

    start = datetime.strptime(str(train_start_ticket), fmt)

    end = datetime.strptime(str(train_end_ticket + 1), fmt)

    gk2a_timelist = []
    # 학습 데이터 10분 단위로 리스트 생성
    for result in perdelta(start , end , timedelta(minutes=10)):
        gk2a_timelist.append(result)
    gk2a_timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , gk2a_timelist ))

    # 이미지 모델을 저장할 경로 생성
    img_model_dir = os.path.join(saving_model_path, "image_model")
    if not os.path.exists(img_model_dir):
        os.makedirs(img_model_dir)

    # 학습 데이터 셔플. 학습 도중 tensorflow api 를 활용해서 셔플하면 심각하게 학습 속도가 저하됨
    from sklearn.utils import shuffle
    gk2a_timelist = shuffle(gk2a_timelist)

    # 학습 데이터 생성 경로 ( 용량이 커서 나스 마운트 경로로 설정 필요)
    training_data_dir = fog_create_data_path.create_img_data_path
    if not os.path.exists(training_data_dir):
        os.makedirs(training_data_dir)

    # 기존 학습 데이터가 남아 있으면 전부 삭제
    previous_data = os.listdir(training_data_dir)
    if len(previous_data) > 0:
        previous_data_path = list(map(lambda x: os.path.join(training_data_dir , x) , previous_data ))
        for f in previous_data_path:
            os.remove(f)


    # 학습 데이터 병렬로 생성
    whole_multi_process_x_data(gk2a_timelist , using_features, training_data_dir)

    # 학습 데이터 feature 저장 ---- 생성한 모델을 최적 모델로 사용하게 되면 이 파일이 필요함
    using_features_path = os.path.join(img_model_dir , "using_features.json")
    with open(using_features_path, "w") as fp:
        json.dump(using_features, fp)
    fp.close()


    return img_model_dir , training_data_dir , using_features_path








if __name__ == '__main__':
    #os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

    train_data_params = {"train_start_ticket":"201910100000",
                         "train_end_ticket"  :"201910100050",
                         "using_features":  ['ir087_Radiance',
                                             #'ir087_Brightness',
                                             'ir096_Radiance',
                                             #'ir096_Brightness',
                                             #'ir105_Radiance',
                                             #'ir105_Brightness',
                                             'ir112_Radiance',
                                             #'ir112_Brightness',
                                             'ir123_Radiance',
                                             #'ir123_Brightness',
                                             'ir133_Radiance',
                                             #'ir133_Brightness',
                                             'nr013_Albedo',
                                             'nr016_Albedo',
                                             'sw038_Radiance',
                                             #'sw038_Brightness',
                                             'vi004_Albedo',
                                             'vi005_Albedo',
                                             'vi006_Albedo',
                                             'vi008_Albedo',
                                             'wv063_Radiance',
                                             #'wv063_Brightness',
                                             'wv069_Radiance',
                                             #'wv069_Brightness',
                                             'wv073_Radiance',
                                             #'wv073_Brightness',
                                             'zenith'],
                                             #'HSR_value'],
                         "model_path": "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance/developing3",
                         "epoch_num" : 110,
                         "batch_size_num":1,
                         "es_patience" : 50,
                         "gan_1":225,
                         "gan_2":450,
                         "gan_3":900,
                         "gan_4":1800,
                         "dnn_1":32,
                         "dnn_2":32,
                         "dnn_3":16,
                         "optimizer":"1",
                         "loss_func":"1",
                         "learning_rate": 0.001,
                         "img_model_test_data_counts":10 ,
                         "num_model_test_data_ratio" :0.1,
                         "num_model_pixel_width" : 3 ,
                         "num_model_pixel_length" : 3}


    log = fog_logging.get_log_view(1, "platform", False, 'advance_create_data_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'advance_create_data_errorlog')

    total_steps , img_model_dir , training_data_dir  = main(train_data_params , log ,error_log)

    print(total_steps)
    print(img_model_dir)
    print(training_data_dir)
