#!/usr/bin/env python

import os




class amos_db_feature_naming(object):
    regular_tower_features = {"K1":"now_time" ,
                              "K2":"target_base" ,
                              "SW1":"wind_dir" ,
                              "SW2":"wind_speed" ,
                              "SU1":"temperature" ,
                              "SU3" : "humid" ,
                              "SU9": "land_pressure" ,
                              "SU11":"sea_pressure" ,
                              "SU13":"rain_hour" ,
                              "SU7":"target" }
    tower_153_features =     {"K1":"now_time" ,
                                "K2":"target_base" ,
                                "SW1":"wind_dir" ,
                                "SW2":"wind_speed" ,
                                "SU1":"temperature" ,
                                "SU3" : "humid" ,
                                "SU12": "land_pressure" ,
                                "SU14":"sea_pressure" ,
                                "SU16":"rain_hour" ,
                                "SU8":"target" }










class preprocess_info(object):
    create_data_cpu_count = 30
    hdfs_nas =  'nas'# 'hdfs' 'nas'
    step_size = 50

    features_min_max = {'ir087_Radiance' : [3.002 , 83.877],
                        'ir087_Brightness' : [191.63 , 309.326],
                        'ir096_Radiance' : [10.336 , 61.047],
                        'ir096_Brightness' : [208.715 , 277.301],
                        'ir105_Radiance' : [6.907 , 129.988],
                        'ir105_Brightness' : [189.086 , 314.043],
                        'ir112_Radiance' : [9.321 , 144.592],
                        'ir112_Brightness' : [188.331 , 314.136],
                        'ir123_Radiance' : [12.985 , 148.963],
                        'ir123_Brightness' : [188.223 , 309.027],
                        'ir133_Radiance' : [17.404 , 113.234],
                        'ir133_Brightness' : [190.771 , 283.181],
                        'nr013_Albedo' : [-0.011 , 0.65],
                        'nr016_Albedo' : [-0.013 , 0.782],
                        'sw038_Radiance' : [-0.002 , 9.434],
                        'sw038_Brightness' : [152.77 , 374.868],
                        'vi004_Albedo' : [-0.011 , 1.136],
                        'vi005_Albedo' : [-0.011 , 1.15],
                        'vi006_Albedo' : [-0.012 , 1.167],
                        'vi008_Albedo' : [-0.012 , 1.213],
                        'wv063_Radiance' : [0.307 , 6.297],
                        'wv063_Brightness' : [192.802 , 258.187],
                        'wv069_Radiance' : [0.648 , 15.771],
                        'wv069_Brightness' : [189.834 , 268.396],
                        'wv073_Radiance' : [0.99 , 23.209],
                        'wv073_Brightness' : [190.134 , 273.735],
                        'zenith' : [33.53 , 159.913],
                        'HSR_value' : [0.0 , 65]  }  # acutal max of max : 1775.646 , 75 percentile of each max : 65

    feature_mean_std = {'ir087_Radiance' : [43.072 , 12.695],
                        'ir087_Brightness' : [273.613 , 15.277],
                        'ir096_Radiance' : [37.048 , 8.367],
                        'ir096_Brightness' : [253.007 , 10.231],
                        'ir105_Radiance' : [71.995 , 18.852],
                        'ir105_Brightness' : [275.716 , 16.073],
                        'ir112_Radiance' : [82.705 , 20.651],
                        'ir112_Brightness' : [275.237 , 16.349],
                        'ir123_Radiance' : [91.847 , 20.676],
                        'ir123_Brightness' : [273.224 , 15.713],
                        'ir133_Radiance' : [82.321 , 14.423],
                        'ir133_Brightness' : [261.082 , 11.707],
                        'nr013_Albedo' : [0.008 , 0.015],
                        'nr016_Albedo' : [0.061 , 0.043],
                        'sw038_Radiance' : [0.416 , 0.195],
                        'sw038_Brightness' : [281.6 , 13.309],
                        'vi004_Albedo' : [0.082 , 0.05],
                        'vi005_Albedo' : [0.078 , 0.053],
                        'vi006_Albedo' : [0.071 , 0.057],
                        'vi008_Albedo' : [0.087 , 0.063],
                        'wv063_Radiance' : [2.931 , 0.673],
                        'wv063_Brightness' : [237.175 , 5.802],
                        'wv069_Radiance' : [7.979 , 1.839],
                        'wv069_Brightness' : [245.851 , 7.269],
                        'wv073_Radiance' : [13.382 , 3.09],
                        'wv073_Brightness' : [253.308 , 8.545],
                        'zenith' : [95.405 , 4.777],
                        'HSR_value' : [0.043 , 0.395]}



class all_basic_path(object):
    root_source_path = "/gisangdan/kans/ai/src/fog-det-trac/fog_det_advance"
    nas_root_path = "/gisangdan/kans/data-ext"
    hdfs_root_path = "/hadoop/hdfs/data/PARSER"
    output_root_path = "/gisangdan/kans/data-int/fog-det-trac"
    backup_model_path = "/gisangdan/kans/ai/model-save/fog-det-trac/backup_models"

class external_info_path(object):
    gk2a_hsr_pixel_info = os.path.join(all_basic_path.root_source_path , "gk2a_hsr_int_pixel.csv")
    JDBC_DRIVER = os.path.join(all_basic_path.root_source_path , "tibero6-jdbc.jar")
    training_amos_info_path = os.path.join(all_basic_path.root_source_path , "training_amos_info.csv")
    all_amos_info_path = os.path.join(all_basic_path.root_source_path , "all_amos_info.csv")

class saving_path(object):
    diff_img_path = os.path.join(all_basic_path.output_root_path, "diff_image")
    tempo_path = os.path.join(all_basic_path.output_root_path,'tempo_file')

class fog_create_model_info(object):
    #train_test_data_ratio = 0.95
    mse_threshold = 0.00001
    checkpoint_interval = 100
    saving_train_pic_duration = 3
    BUFFER_SIZE = 400
    LAMBDA = 100

    nm_model_array_width = 3
    nm_model_array_length =  3



class fog_create_data_path(object):
    gk2a_fog_img_path = os.path.join(all_basic_path.nas_root_path , "GK2A/FOG_IMAGE/work/{}/gk2a_ami_le2_fog_ko020lc_{}.png")  # yearmonth , yearmonthdayhourmin

    gk2a_array_nas_folder = os.path.join(all_basic_path.nas_root_path , "GK2A/ko/result/{}/{}/{}/result/gk2a_ami_le1b_{}_ko0{}lc_{}.nc/")  # yearmonth #day #hour #channel  #size #ticket
    gk2a_array_hdfs_path  = os.path.join(all_basic_path.hdfs_root_path , "GK2A/{}/{}/{}/gk2a_ami_le1b_{}_ko0{}lc_{}.nc.{}") #capital channel #yearmonth  #day #channel #size #ticketNo  #feature type (Brightness_Temperature)

    hsr_array_nas_path  = os.path.join(all_basic_path.nas_root_path , "HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt") #yearmonth #day #ticketNo #ticketNo
    hsr_array_hdfs_path = os.path.join(all_basic_path.hdfs_root_path , "HSR/{}/{}/RDR_CMP_HSR_PUB_{}.bin") #yearmonth #day #ticketNo

    hsr_mapping_table = os.path.join(all_basic_path.root_source_path , "gk2a_hsr_int_pixel.csv")
    latlon_900_900 = os.path.join(all_basic_path.root_source_path , "latlon_gk2a/LATLON_KO_2000.txt" )

    create_img_data_path = os.path.join(all_basic_path.nas_root_path , "fog_traindata/img_model/training_data")
    create_num_data_path = os.path.join(all_basic_path.nas_root_path , "fog_traindata/num_model/training_data")


class gk2a_fog_img_attrs(object):
    diff1_rgba = [255,0,  0,255]
    diff2_rgba = [192,0,  0,255]
    diff3_rgba = [255,192,0,255]
    diff4_rgba = [255,255,0,255]
    diff5_rgba = [0,  176,80,255]
    diff6_rgba = [146,208,80,255]
    no_diff_rgba = 0

class hdfs_path_class(object):
    #normal_hdfs_path = "http://192.168.0.179:50070" # "http://ba01.t3q.com:50070"

    #backup_hdfs_path = "http://192.168.0.178:50070" # "http://ba02.t3q.com:50070"
    hdfs_port = "50070"

    ambari_host = "192.168.0.174"
    auth_pass = 'admin'
    ambari_cluster_name = 'hacluster'
    auth_user = 'admin'



class tb_info(object):
    tb_host =  '192.168.0.18'# '172.16.200.10'
    tb_db_name = 'tibero'
    tb_user_name = 'afw'  # 'afns'
    tb_password =  'afw123'   #'Qwerty0987@'
    tb_port = 8629

class fog_class(object):
    deep_fog_high_end = 835
    normal_fog_high_end = 1670
    weak_fog_high_end = 5010

class fog_character(object):
    maximum_prediction_time = 10800
    connectivity = 8
    coord_count = 20
    intersection_ratio  = 0.4

    deep_fog_high_end = fog_class.deep_fog_high_end
    normal_fog_high_end = fog_class.normal_fog_high_end
    fog_binary_level = fog_class.weak_fog_high_end

    maximum_vis_distance_clipping = 7000
    maximum_fog_speed = 80
    vis_distance_digits_number = 3

class fog_coloring(object):
    deep_fog_rgb = [255,0,0,255]
    normal_fog_rgb = [255,69,0,255]
    weak_fog_rgb = [255,255,0,255]


class fog_final_db_info(object):
    watch_warn_limit = 1609
    past_valid_time_limit = 30  ## 날짜
    null_data = -50000

class fog_img_making(object):
    expanding_arrow_color = (100, 0, 190 , 255)
    reduction_arraw_color = (0, 255 , 0 , 255)
    same_arrw_color = (50 , 160 , 50 , 255)
    ### time image at top ###
    time_img_shape=  (30, 900 , 4)
    w_color_time        = (255, 255, 255, 255)
    thickness_time = 2
    fontScale_time = 0.6
    location_time = (10,23)

    input_txt = "Now time : {0} (KST)"

    ### base_info ###
    base_rgb = (0 , 225 , 150 , 255)
    base_tickness  = 2
    base_cross_length = 5

    ## arrow_info ###
    expanding_arrow_color = (100, 0, 190 , 255)
    reduction_arraw_color = (0, 255 , 0 , 255)
    same_arrw_color = (50 , 160 , 50 , 255)
    arrow_thickness = 2
    arrow_tiplength = 0.1

    ## not moving fog info ###
    fog_cross_length = 5
    ## cutting information ##
    # back image
    root_path = all_basic_path.root_source_path
    bk_img_path = "{}/inverted_bk_img.png".format(root_path)
    pred_img_path_base = '/gisangdan/kans/data-int/fog-det-trac/image/{}/fog-det-trac_{}_image-det.png'
    below_legend_img_path = "{}/flat_info2.png".format(root_path)

    moving_speed_class = [0 , 10, 20, 30 , 40, 50 , 60 , 70 , 80]
    moving_speed_class.sort()
    moving_speed_distance = {}
    distance = 0
    for moving_class in moving_speed_class:
        moving_speed_distance[moving_class] = distance
        distance = distance + 5


# class custom_features(object):
#     using_features  = ['ir087_Radiance',       #'ir087_Effective',
#                     'ir087_Brightness',
#                     'ir096_Radiance',
#                     'ir096_Brightness',
#                     'ir105_Radiance',
#                     'ir105_Brightness',
#                     'ir112_Radiance',
#                     'ir112_Brightness',
#                     'ir123_Radiance',
#                     'ir123_Brightness',
#                     'ir133_Radiance',
#                     'ir133_Brightness',
#                     'nr013_Albedo',
#                     'nr016_Albedo',
#                     'sw038_Radiance',
#                     'sw038_Brightness',
#                     #'vi004_Radiance',
#                     'vi004_Albedo',
#                     #'vi005_Radiance',
#                     'vi005_Albedo',
#                     #'vi006_Radiance',
#                     'vi006_Albedo',
#                     #'vi008_Radiance',
#                     'vi008_Albedo',
#                     'wv063_Radiance',
#                     'wv063_Brightness',
#                     'wv069_Radiance',
#                     'wv069_Brightness',
#                     'wv073_Radiance',
#                     'wv073_Brightness',
#                     'zenith',
#                     'HSR_value']
#
#
# class fixed_features(object):
#     using_features = ['ir087_Radiance',       #'ir087_Effective',
#                     'ir087_Brightness',
#                     'ir096_Radiance',
#                     'ir096_Brightness',
#                     'ir105_Radiance',
#                     'ir105_Brightness',
#                     'ir112_Radiance',
#                     'ir112_Brightness',
#                     'ir123_Radiance',
#                     'ir123_Brightness',
#                     'ir133_Radiance',
#                     'ir133_Brightness',
#                     'nr013_Albedo',
#                     'nr016_Albedo',
#                     'sw038_Radiance',
#                     'sw038_Brightness',
#                     #'vi004_Radiance',
#                     #'vi004_Albedo',
#                     #'vi005_Radiance',
#                     #'vi005_Albedo',
#                     #'vi006_Radiance',
#                     #'vi006_Albedo',
#                     #'vi008_Radiance',
#                     #'vi008_Albedo',
#                     'wv063_Radiance',
#                     'wv063_Brightness',
#                     'wv069_Radiance',
#                     'wv069_Brightness',
#                     'wv073_Radiance',
#                     'wv073_Brightness',
#                     'zenith',
#                     'HSR_value']
