import os
import json
from datetime import datetime , timedelta
import numpy as np
import pandas as pd
from fog_config import fog_create_data_path, hdfs_path_class , preprocess_info

from hdfs import InsecureClient
import requests
import fog_logging


def check_data_hdfs(path , hdfs_port , ambari_host , auth_pass , ambari_cluster_name, auth_user, log , error_log ):
    try:
        hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user)
        if hdfs_status  !=  200:
            log.info("hdfs status : {}".format(hdfs_status))
            return False
        else:
            working_hdfs_path = "http://{}:{}".format(active_host , hdfs_port)
            client = InsecureClient(working_hdfs_path, user="hdfs")
            file_status = client.status(path , strict = False)
            if file_status == None:
                return False
            else:
                return True
    except Exception as e:
        error_log.error("unknow error occured in checking the data : {}".format(e))
        return False



def check_x_data_nas(ticketNo, using_features , log ,error_log):
    status = "good"
    try:
        for feature in using_features:
            log.info(feature)
            if feature == "HSR_value":
                status = check_hsr_array_nas(ticketNo)
            elif feature == "zenith":
                pass
            else:
                status = check_gk2a_array_nas(ticketNo , feature)

            if status == "bad":
                log.info("not enough data at {}".format(ticketNo))
                log.info("missing feature : {}".format(feature))
                break
        return status
    except Exception as e:
        error_log.error("error reading ticket at : {} ----> {}".format(ticketNo , e))


def check_hsr_array_nas(ticketNo):
    try:
        kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
        kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
        kr_yearmonth = kr_ticketNo[:6]
        kr_day = kr_ticketNo[6:8]
        file_path = fog_create_data_path.hsr_array_nas_path.format(kr_yearmonth , kr_day , kr_ticketNo, kr_ticketNo)
        if os.path.isfile(file_path):
            return "good"
        else:
            return "bad"
    except:
        return "bad"

def check_gk2a_array_nas(ticketNo , feature):
    try:
        yearmonth = ticketNo[:6]
        day = ticketNo[6:8]
        hour = ticketNo[8:10]
        channel = feature.split("_")[0]
        if "vi006" in feature:
            size = "05"
        elif "vi" in feature:
            size = "10"
        else:
            size = "20"
        folder_path = fog_create_data_path.gk2a_array_nas_folder.format(yearmonth , day , hour, channel ,size , ticketNo)
        file_list = os.listdir(folder_path)
        key_word1 =  "nc." + feature.split("_")[1]
        key_word2 =  "nc" + feature.split("_")[1]
        available_file = list(filter(lambda x: key_word1 in x or key_word2 in x , file_list ))[0]
        final_file_path = os.path.join(folder_path , available_file)
        if os.path.isfile(final_file_path):
            return "good"
        else:
            return "bad"
    except:
        return "bad"



def check_x_data_nas(ticketNo, using_features , log ,error_log):
    nas_status = "good"
    try:
        for feature in using_features:
            log.info(feature)
            if feature == "HSR_value":
                nas_status = check_hsr_array_nas(ticketNo)
            elif feature == "zenith":
                pass
            else:
                nas_status = check_gk2a_array_nas(ticketNo , feature)

            if nas_status == "bad":
                log.info("not enough data at {}".format(ticketNo))
                log.info("missing feature : {}".format(feature))
                break
        return nas_status
    except Exception as e:
        error_log.error("error reading ticket at : {} ----> {}".format(ticketNo , e))


def check_x_data_hdfs(ticketNo , using_features  , log , error_log):
    hdfs_status = "good"
    try:
        ambari_host = hdfs_path_class.ambari_host
        auth_pass = hdfs_path_class.auth_pass
        ambari_cluster_name = hdfs_path_class.ambari_cluster_name
        auth_user = hdfs_path_class.auth_user
        hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass ,ambari_cluster_name ,auth_user)
        if hdfs_status != 200:
            log.info("hdfs status : {}".format(hdfs_status))
        working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)
        client = InsecureClient(working_hdfs_path, user="hdfs")
        for feature in using_features:
            log.info(feature)
            if feature == "HSR_value":
                hdfs_status = check_hsr_array_hdfs(ticketNo ,client)
            elif feature == "zenith":
                pass
            else:
                hdfs_status =  check_gk2a_array_hdfs(ticketNo , feature, client)

            if hdfs_status == "bad":
                log.info("not enough data at {}".format(ticketNo))
                log.info("missing feature : {}".format(feature))
                break
        return hdfs_status
    except Exception as e:
        error_log.error("error reading ticket at : {} --->{}".format(ticketNo , e))


def getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user):
    active_host = None



    api_url = 'http://{}:8080/api/v1/clusters/{}/host_components?HostRoles/component_name=NAMENODE&metrics/dfs/FSNamesystem/HAState=active'.format(ambari_host,
                                                                                                                                                   ambari_cluster_name)
    res = requests.get(api_url, auth=(auth_user, auth_pass))

    status_code = res.status_code
    res_json = res.json()

    if status_code==200 and res_json['items']:
        active_host = res_json['items'][0]['HostRoles']['host_name']

    return status_code, active_host

def check_gk2a_array_hdfs(ticketNo , feature, client):
    try:
        yearmonth = ticketNo[:6]
        day = ticketNo[6:8]
        channel , feature_type = feature.split("_")
        if "vi006" in feature:
            size = "05"
        elif "vi" in feature:
            size = "10"
        else:
            size = "20"
        if feature_type == "Brightness" : feature_type = "Brightness_Temperature"
        file_path = fog_create_data_path.gk2a_array_hdfs_path.format(channel.upper() ,yearmonth , day ,  channel , size , ticketNo , feature_type)
        file_status = client.status(file_path,  strict = False)
        if file_status  == None:
            log.info("no file found at : {}".format(file_path))
            return "bad"
        else:
            return "good"
    except:
        return "bad"




def check_hsr_array_hdfs(ticketNo ,client):
    try:
        kr_date = datetime.strptime(ticketNo, '%Y%m%d%H%M') + timedelta(minutes=540)
        kr_ticketNo = kr_date.strftime('%Y%m%d%H%M')
        kr_yearmonth = kr_ticketNo[:6]
        kr_day = kr_ticketNo[6:8]
        file_path = fog_create_data_path.hsr_array_hdfs_path.format(kr_yearmonth , kr_day , kr_ticketNo)
        status = client.status(file_path,  strict = False)
        if status  == None:
            log.info("no file found at : {}".format(file_path))
            return "bad"
        else:
            return "good"
    except:
        return "bad"



def main(saving_ticketNo ,  using_features , model_path):
    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_check_data_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_check_data_error')

    import json
    processing_path = '{}/processing_status.json'.format(model_path)
    with open(processing_path) as json_file:
        status = json.load(json_file)
    json_file.close()
    log.info("processing status : {}".format(status["processing"]))

    if status["processing"] == "True":
        log.info("previous process is not finished --------> stopping process")
        return True, saving_ticketNo

    if preprocess_info.hdfs_nas == "nas":
        check_data_func = check_x_data_nas
    else:
        check_data_func = check_x_data_hdfs


    ticketNo_date = datetime.strptime(saving_ticketNo, '%Y%m%d%H%M') - timedelta(minutes=20)
    reading_ticketNo = ticketNo_date.strftime('%Y%m%d%H%M')
    first_try = check_data_func(reading_ticketNo, using_features , log ,error_log)

    if first_try == "good":
        return False, reading_ticketNo

    else:
        ticketNo_date = datetime.strptime(saving_ticketNo, '%Y%m%d%H%M') - timedelta(minutes=30)
        reading_ticketNo = ticketNo_date.strftime('%Y%m%d%H%M')
        second_try = check_data_func(reading_ticketNo, using_features , log ,error_log)

    if second_try == "good":
        return False  , reading_ticketNo

    else:
        return True , saving_ticketNo
